% CBEP_model_data_comp
% 
% This script takes the ocean temperature data from the models for the 9
% proxy data sites used in the paper
% 

CBEP_loaddata

% Site locations in model space

% Kerguelen Plateau:
KPlon = [20:22];
KPlat = [12:14];
KPdpth = [7:11];

% Falklands Plateau:
FPlon = [85:87];
FPlat = [15:17];
FPdpth = [1:5];

% New Zealand:
NZlon = [49:51];
NZlat = [12:14];
NZdpth = [1:5];

% Agulhas Ridge
AGlon = [1,2,96];
AGlat = [17:19];
AGdpth = [1:5];

% North Atlantic:
NAlon = [92:94];
NAlat = [60:62];
NAdpth = [1:5];

% Arctic:
ARlon = [1:3];
ARlat = [64:66];
ARdpth = [1:5];

% Ceara Rise:
CRlon = [85:87];
CRlat = [37:39];
CRdpth = [1:5];

% Tanzania:
TZlon = [9:11];
TZlat = [30:32];
TZdpth = [1:5];

% St Stephen's Quarry:
SQlon = [74:76];
SQlat = [48:50];
SQdpth = [1:5];



%% Find ocean temperatures around each proxy site

% Identifiers for simulations
desc = {'cha4nI','cha4eI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
    'rup2nI','rup2eI','rup2fI','pri2nI','pri2fI','cha4fI','pri4nI','pri4fI'};

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = desc(i);
    desc_n = sprintf('%s',desc_cell{:});
    desc_mean = [desc_n,'_mean'];
    

    % Find difference
    temp = OceanT.(desc_n);
    KP.(desc_n) = temp(KPlon,KPlat,KPdpth);
    FP.(desc_n) = temp(FPlon,FPlat,FPdpth);
    NZ.(desc_n) = temp(NZlon,NZlat,NZdpth);
    AG.(desc_n) = temp(AGlon,AGlat,AGdpth);
    NA.(desc_n) = temp(NAlon,NAlat,NAdpth);
    AR.(desc_n) = temp(ARlon,ARlat,ARdpth);
    CR.(desc_n) = temp(CRlon,CRlat,CRdpth);
    TZ.(desc_n) = temp(TZlon,TZlat,TZdpth);
    SQ.(desc_n) = temp(SQlon,SQlat,SQdpth);
    
    KP.(desc_mean) = nanmean(nanmean(nanmean(temp(KPlon,KPlat,KPdpth))));
    FP.(desc_mean) = nanmean(nanmean(nanmean(temp(FPlon,FPlat,FPdpth))));
    NZ.(desc_mean) = nanmean(nanmean(nanmean(temp(NZlon,NZlat,NZdpth))));
    AG.(desc_mean) = nanmean(nanmean(nanmean(temp(AGlon,AGlat,AGdpth))));
    NA.(desc_mean) = nanmean(nanmean(nanmean(temp(NAlon,NAlat,NAdpth))));
    AR.(desc_mean) = nanmean(nanmean(nanmean(temp(ARlon,ARlat,ARdpth))));
    CR.(desc_mean) = nanmean(nanmean(nanmean(temp(CRlon,CRlat,CRdpth))));
    TZ.(desc_mean) = nanmean(nanmean(nanmean(temp(TZlon,TZlat,TZdpth))));
    SQ.(desc_mean) = nanmean(nanmean(nanmean(temp(SQlon,SQlat,SQdpth))));

end


%% Find mean effect of Full AIS growth
% Identifiers for glaciated simulations with an ice free pair
desc = {'cha2fI','cha4fI','rup2fI','rup4fI','pri2fI','pri4fI','cha2eI','cha4eI','rup2eI'};

% Make empty array to store each comparison
KPmeanice = nan(3,length(desc));
FPmeanice = nan(3,length(desc));
NZmeanice = nan(3,length(desc));
AGmeanice = nan(3,length(desc));
NAmeanice = nan(3,length(desc));
ARmeanice = nan(3,length(desc));
CRmeanice = nan(3,length(desc));
TZmeanice = nan(3,length(desc));
SQmeanice = nan(3,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = desc(i);
    desc_f = [sprintf('%s',desc_cell{:}),'_mean'];
    
    % Find 4x CO2 identifiers
    desc_n = [desc_f(1:4),'n',desc_f(6:11)];

    % Find difference at each site
    KPmeanice(1,i) = KP.(desc_n);
    FPmeanice(1,i) = FP.(desc_n);
    NZmeanice(1,i) = NZ.(desc_n);
    AGmeanice(1,i) = AG.(desc_n);
    NAmeanice(1,i) = NA.(desc_n);
    ARmeanice(1,i) = AR.(desc_n);
    CRmeanice(1,i) = CR.(desc_n);
    TZmeanice(1,i) = TZ.(desc_n);
    SQmeanice(1,i) = SQ.(desc_n);
    
    KPmeanice(2,i) = KP.(desc_f);
    FPmeanice(2,i) = FP.(desc_f);
    NZmeanice(2,i) = NZ.(desc_f);
    AGmeanice(2,i) = AG.(desc_f);
    NAmeanice(2,i) = NA.(desc_f);
    ARmeanice(2,i) = AR.(desc_f);
    CRmeanice(2,i) = CR.(desc_f);
    TZmeanice(2,i) = TZ.(desc_f);
    SQmeanice(2,i) = SQ.(desc_f);
    
    KPmeanice(3,i) = KPmeanice(2,i) - KPmeanice(1,i);
    FPmeanice(3,i) = FPmeanice(2,i) - FPmeanice(1,i);
    NZmeanice(3,i) = NZmeanice(2,i) - NZmeanice(1,i);
    AGmeanice(3,i) = AGmeanice(2,i) - AGmeanice(1,i);
    NAmeanice(3,i) = NAmeanice(2,i) - NAmeanice(1,i);
    ARmeanice(3,i) = ARmeanice(2,i) - ARmeanice(1,i);
    CRmeanice(3,i) = CRmeanice(2,i) - CRmeanice(1,i);
    TZmeanice(3,i) = TZmeanice(2,i) - TZmeanice(1,i);
    SQmeanice(3,i) = SQmeanice(2,i) - SQmeanice(1,i);
    
end


%% Find mean effect of paleogeographic change
desc = {'cha4nI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI','rup2nI','rup2fI'};

KPmeangeog = nan(3,length(desc));
FPmeangeog = nan(3,length(desc));
NZmeangeog = nan(3,length(desc));
AGmeangeog = nan(3,length(desc));
NAmeangeog = nan(3,length(desc));
ARmeangeog = nan(3,length(desc));
CRmeangeog = nan(3,length(desc));
TZmeangeog = nan(3,length(desc));
SQmeangeog = nan(3,length(desc));

for i = 1:length(desc)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Find previous equivalent palaeogeog
    if desc_str(1) == 'c'
        descdif = ['rup',desc_str(4:6),'_mean'];
    else if desc_str(1) == 'r'
            descdif = ['pri',desc_str(4:6),'_mean'];
        end
    end
    
    desc_str2 = [desc_str,'_mean'];
    
    % Find difference at each site
    KPmeangeog(1,i) = KP.(descdif);
    FPmeangeog(1,i) = FP.(descdif);
    NZmeangeog(1,i) = NZ.(descdif);
    AGmeangeog(1,i) = AG.(descdif);
    NAmeangeog(1,i) = NA.(descdif);
    ARmeangeog(1,i) = AR.(descdif);
    CRmeangeog(1,i) = CR.(descdif);
    TZmeangeog(1,i) = TZ.(descdif);
    SQmeangeog(1,i) = SQ.(descdif);
    
    KPmeangeog(2,i) = KP.(desc_str2);
    FPmeangeog(2,i) = FP.(desc_str2);
    NZmeangeog(2,i) = NZ.(desc_str2);
    AGmeangeog(2,i) = AG.(desc_str2);
    NAmeangeog(2,i) = NA.(desc_str2);
    ARmeangeog(2,i) = AR.(desc_str2);
    CRmeangeog(2,i) = CR.(desc_str2);
    TZmeangeog(2,i) = TZ.(desc_str2);
    SQmeangeog(2,i) = SQ.(desc_str2);
    
    KPmeangeog(3,i) = KPmeangeog(2,i) - KPmeangeog(1,i);
    FPmeangeog(3,i) = FPmeangeog(2,i) - FPmeangeog(1,i);
    NZmeangeog(3,i) = NZmeangeog(2,i) - NZmeangeog(1,i);
    AGmeangeog(3,i) = AGmeangeog(2,i) - AGmeangeog(1,i);
    NAmeangeog(3,i) = NAmeangeog(2,i) - NAmeangeog(1,i);
    ARmeangeog(3,i) = ARmeangeog(2,i) - ARmeangeog(1,i);
    CRmeangeog(3,i) = CRmeangeog(2,i) - CRmeangeog(1,i);
    TZmeangeog(3,i) = TZmeangeog(2,i) - TZmeangeog(1,i);
    SQmeangeog(3,i) = SQmeangeog(2,i) - SQmeangeog(1,i);

end




%% Find mean effect of pCO2 decline

% Identifiers for 2x icehouse simulations with a 4x CO2 pair
desc = {'cha2nI','cha2eI','cha2fI','rup2nI','rup2fI','pri2nI','pri2fI'};

% Make empty array to store each CO2 comparison
KPmeanCO2 = nan(3,length(desc));
FPmeanCO2 = nan(3,length(desc));
NZmeanCO2 = nan(3,length(desc));
AGmeanCO2 = nan(3,length(desc));
NAmeanCO2 = nan(3,length(desc));
ARmeanCO2 = nan(3,length(desc));
CRmeanCO2 = nan(3,length(desc));
TZmeanCO2 = nan(3,length(desc));
SQmeanCO2 = nan(3,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = desc(i);
    desc_2x = [sprintf('%s',desc_cell{:}),'_mean'];
    
    % Find 4x CO2 identifiers
    desc_4x = [desc_2x(1:3),'4',desc_2x(5:11)];

    % Find difference at each site
    KPmeanCO2(1,i) = KP.(desc_4x);
    FPmeanCO2(1,i) = FP.(desc_4x);
    NZmeanCO2(1,i) = NZ.(desc_4x);
    AGmeanCO2(1,i) = AG.(desc_4x);
    NAmeanCO2(1,i) = NA.(desc_4x);
    ARmeanCO2(1,i) = AR.(desc_4x);
    CRmeanCO2(1,i) = CR.(desc_4x);
    TZmeanCO2(1,i) = TZ.(desc_4x);
    SQmeanCO2(1,i) = SQ.(desc_4x);
    
    KPmeanCO2(2,i) = KP.(desc_2x);
    FPmeanCO2(2,i) = FP.(desc_2x);
    NZmeanCO2(2,i) = NZ.(desc_2x);
    AGmeanCO2(2,i) = AG.(desc_2x);
    NAmeanCO2(2,i) = NA.(desc_2x);
    ARmeanCO2(2,i) = AR.(desc_2x);
    CRmeanCO2(2,i) = CR.(desc_2x);
    TZmeanCO2(2,i) = TZ.(desc_2x);
    SQmeanCO2(2,i) = SQ.(desc_2x);
    
    KPmeanCO2(3,i) = KPmeanCO2(2,i) - KPmeanCO2(1,i);
    FPmeanCO2(3,i) = FPmeanCO2(2,i) - FPmeanCO2(1,i);
    NZmeanCO2(3,i) = NZmeanCO2(2,i) - NZmeanCO2(1,i);
    AGmeanCO2(3,i) = AGmeanCO2(2,i) - AGmeanCO2(1,i);
    NAmeanCO2(3,i) = NAmeanCO2(2,i) - NAmeanCO2(1,i);
    ARmeanCO2(3,i) = ARmeanCO2(2,i) - ARmeanCO2(1,i);
    CRmeanCO2(3,i) = CRmeanCO2(2,i) - CRmeanCO2(1,i);
    TZmeanCO2(3,i) = TZmeanCO2(2,i) - TZmeanCO2(1,i);
    SQmeanCO2(3,i) = SQmeanCO2(2,i) - SQmeanCO2(1,i);
  
end


%% Find mean effect of pCO2 drop + ice growth

% Identifiers for 2x glaciated simulations with a 4x CO2 unglaciated pair
desc = {'cha2eI','cha2fI','rup2eI','rup2fI','pri2fI'};

% Make empty array to store each CO2 comparison
KPmeanEOT = nan(3,length(desc));
FPmeanEOT = nan(3,length(desc));
NZmeanEOT = nan(3,length(desc));
AGmeanEOT = nan(3,length(desc));
NAmeanEOT = nan(3,length(desc));
ARmeanEOT = nan(3,length(desc));
CRmeanEOT = nan(3,length(desc));
TZmeanEOT = nan(3,length(desc));
SQmeanEOT = nan(3,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = desc(i);
    desc_2x = [sprintf('%s',desc_cell{:}),'_mean'];
    
    % Find 4x CO2 identifiers
    desc_4x = [desc_2x(1:3),'4n',desc_2x(6:11)];

    % Find difference at each site
    KPmeanEOT(1,i) = KP.(desc_4x);
    FPmeanEOT(1,i) = FP.(desc_4x);
    NZmeanEOT(1,i) = NZ.(desc_4x);
    AGmeanEOT(1,i) = AG.(desc_4x);
    NAmeanEOT(1,i) = NA.(desc_4x);
    ARmeanEOT(1,i) = AR.(desc_4x);
    CRmeanEOT(1,i) = CR.(desc_4x);
    TZmeanEOT(1,i) = TZ.(desc_4x);
    SQmeanEOT(1,i) = SQ.(desc_4x);
    
    KPmeanEOT(2,i) = KP.(desc_2x);
    FPmeanEOT(2,i) = FP.(desc_2x);
    NZmeanEOT(2,i) = NZ.(desc_2x);
    AGmeanEOT(2,i) = AG.(desc_2x);
    NAmeanEOT(2,i) = NA.(desc_2x);
    ARmeanEOT(2,i) = AR.(desc_2x);
    CRmeanEOT(2,i) = CR.(desc_2x);
    TZmeanEOT(2,i) = TZ.(desc_2x);
    SQmeanEOT(2,i) = SQ.(desc_2x);
    
    KPmeanEOT(3,i) = KPmeanEOT(2,i) - KPmeanEOT(1,i);
    FPmeanEOT(3,i) = FPmeanEOT(2,i) - FPmeanEOT(1,i);
    NZmeanEOT(3,i) = NZmeanEOT(2,i) - NZmeanEOT(1,i);
    AGmeanEOT(3,i) = AGmeanEOT(2,i) - AGmeanEOT(1,i);
    NAmeanEOT(3,i) = NAmeanEOT(2,i) - NAmeanEOT(1,i);
    ARmeanEOT(3,i) = ARmeanEOT(2,i) - ARmeanEOT(1,i);
    CRmeanEOT(3,i) = CRmeanEOT(2,i) - CRmeanEOT(1,i);
    TZmeanEOT(3,i) = TZmeanEOT(2,i) - TZmeanEOT(1,i);
    SQmeanEOT(3,i) = SQmeanEOT(2,i) - SQmeanEOT(1,i);
    
end

