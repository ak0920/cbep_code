% CBEP_model_data_comp_plot
% 
% Generate CBEP paper Figure 6. This requires the subscript
% CBEP_model_data_subplot to save a lot of copy-pasting...
% 
% The proxy details are outlined in supplementary table 1.
% 
% 

% Load data
CBEP_model_data_comp

figure

%% Sub Arctic
% Proxy details
post = 11.5;
pre = 18.3;
err = 2.6;
% Model details
varice = ARmeanice;
vargeog = ARmeangeog;
varCO2 = ARmeanCO2;
varEOT = ARmeanEOT;
% Plot details
axmax = 19;
axmin = -1;

subplot('Position', [0.075 0.75 0.25 0.25])

hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Sub-Arctic ODP 913 (70 �N)','fontname',fontnm, 'fontsize',fontsz)


%% High North Atlantic
% Proxy details
post = 17.9;
pre = 20;
err = 2;
% Model details
varice = NAmeanice;
vargeog = NAmeangeog;
varCO2 = NAmeanCO2;
varEOT = NAmeanEOT;
% Plot details
axmax = 20;
axmin = 0;

subplot('Position', [0.4 0.75 0.25 0.25])

hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('High North Atlantic ODP 336 (60 �N)','fontname',fontnm, 'fontsize',fontsz)


%% St Stephen's Quarry
% Proxy details
pre = 30.8; % Average from TEX and MgCa data (34.1-33.8 Ma)
post = 28; % Average from TEX and MgCa data (33.7-33.5 Ma)
err = 0; % No data for this site
% Model details
varice = SQmeanice;
vargeog = SQmeangeog;
varCO2 = SQmeanCO2;
varEOT = SQmeanEOT;
% Plot details
axmax = 33;
axmin = 21;

subplot('Position', [0.725 0.75 0.25 0.25])

hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('St Stephens Quarry (30 �N)','fontname',fontnm, 'fontsize',fontsz)


%% Ceara Rise
% Proxy details
post = 27.8;
pre = 28.3;
err = 2.3;
% Model details
varice = CRmeanice;
vargeog = CRmeangeog;
varCO2 = CRmeanCO2;
varEOT = CRmeanEOT;
% Plot details
axmax = 36;
axmin = 24;

subplot('Position', [0.075 0.45 0.25 0.25])
hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Ceara Rise ODP 925 (2.5 �N)','fontname',fontnm, 'fontsize',fontsz)


%% Tanzania
% Proxy details
post = 29.2; % TEX86 data
pre = 31.5; % TEX86 data
err = 0; % No data for this site
% Model details
varice = TZmeanice;
vargeog = TZmeangeog;
varCO2 = TZmeanCO2;
varEOT = TZmeanEOT;
% Plot details
axmax = 36;
axmin = 24;

subplot('Position', [0.4 0.45 0.25 0.25])

hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Tanzania Drilling Project (15 �S)','fontname',fontnm, 'fontsize',fontsz)


%% Agulhas Ridge
% Proxy details
post = 20;
pre = 23.6;
err = 2.5;
% Model details
varice = AGmeanice;
vargeog = AGmeangeog;
varCO2 = AGmeanCO2;
varEOT = AGmeanEOT;
% Plot details
axmax = 24;
axmin = 12;

subplot('Position', [0.725 0.45 0.25 0.25])
hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Agulhas Ridge ODP 1090 (47.5 �S)','fontname',fontnm, 'fontsize',fontsz)


%% Falklands Plateau
% Proxy details
post = (11.1 + 11.2)/2; % TEX86 and Uk'37 average
pre = (18.4 + 19.5)/2; % TEX86 and Uk'37 average
err = 2.5;
% Model details
varice = FPmeanice;
vargeog = FPmeangeog;
varCO2 = FPmeanCO2;
varEOT = FPmeanEOT;
% Plot details
axmax = 24;
axmin = 4;

subplot('Position', [0.075 0.15 0.25 0.25])
hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Falklands Plateau ODP 511 (52.5 �S)','fontname',fontnm, 'fontsize',fontsz)


%% New Zealand
% Proxy details
post = (22.4 + 23.8)/2; % TEX86 and Uk'37 average
pre = (25.6 + 25.9)/2; % TEX86 and Uk'37 average
err = 1.3;
% Model details
varice = NZmeanice;
vargeog = NZmeangeog;
varCO2 = NZmeanCO2;
varEOT = NZmeanEOT;
% Plot details
axmax = 27;
axmin = 7;

subplot('Position', [0.4 0.15 0.25 0.25])
hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('New Zealand ODP 277 (60 �S)','fontname',fontnm, 'fontsize',fontsz)


%% Kerguelen Plateau
% Proxy details
pre = 13.6; % Based on the temperature of Petersen & Schrag 2015 for 689 (12.3�C; Table 2, pre transition) + 2�C (from Bohaty et al. 2012 section 4.1)
post = 11; % Based on the above, minus the change in Bohaty et al. 2012 (section 4.2)
err = 0.8; % From Bohaty et al. 2012 (section 4.2)
% Model details
varice = KPmeanice;
vargeog = KPmeangeog;
varCO2 = KPmeanCO2;
varEOT = KPmeanEOT;
% Plot details
axmax = 20;
axmin = 0;

subplot('Position', [0.725 0.15 0.25 0.25])
hold on

% Do the main plotting
CBEP_model_data_subplot

% Tidy up
title('Kerguelen Plateau (60 �S)','fontname',fontnm, 'fontsize',fontsz)



%% Mannually add legend
leg1ax = axes('Position', [0.15 0.0175 0.7 0.1]);
hold on

plot(6.3,3.4,'o','markersize',size1,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) % pri4nI
plot(6.5,3.4,'s','markersize',size2,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) % pri4nI
plot([6.425 6.575],[3.7 3.1],'-','color',blue,'linewidth',3);

plot(6.3,2.4,'o','markersize',size1,'MarkerFaceColor',red,'MarkerEdgeColor',red) % pri4nI
plot(6.5,2.4,'s','markersize',size2,'MarkerFaceColor',red,'MarkerEdgeColor',red) % pri4nI
plot([6.425 6.575],[2.7 2.1],'-','color',red,'linewidth',3);

plot(6.3,1.4,'o','markersize',size1,'MarkerFaceColor',grey,'MarkerEdgeColor',grey) % pri4nI
plot(6.5,1.4,'s','markersize',size2,'MarkerFaceColor',grey,'Color',grey)
plot([6.425 6.575],[1.7 1.0],'-','color',grey,'linewidth',3);

plot(6.3,0.4,'o','markersize',size1,'MarkerFaceColor',orange,'MarkerEdgeColor',orange) % pri4nI
plot(6.5,0.4,'s','markersize',size2,'MarkerFaceColor',orange,'Color',orange)
plot([6.425 6.575],[0.7 0.1],'-','color',orange,'linewidth',3);


text(4.85, 4.5,'Model forcing (colour)', 'fontname',fontnm, 'fontsize',fontsz)

text(4.5, 3.5,'Antarctic ice growth:', 'fontname',fontnm, 'fontsize',fontsz-1)
text(4.5, 2.5,'pCO_2 reduction:', 'fontname',fontnm, 'fontsize',fontsz-1)
text(4.5, 1.5,'Paleogeog. change:', 'fontname',fontnm, 'fontsize',fontsz-1)
text(4.5, 0.5,'Ice + pCO_2:', 'fontname',fontnm, 'fontsize',fontsz-1)
% text(3, 3,'No ice / Glaciated', 'fontname',fontnm, 'fontsize',fontsz)

text(1, 4.5,'Individual model pair:', 'fontname',fontnm, 'fontsize',fontsz)
plot(3.8,4.5,'o','markersize',size1,'MarkerFaceColor',grey,'MarkerEdgeColor',grey) % pri4nI

text(1, 3.5,'Mean of all model pairs with U_t:', 'fontname',fontnm, 'fontsize',fontsz)
plot(3.8,3.5,'s','markersize',size2,'MarkerFaceColor',grey,'MarkerEdgeColor',grey) % pri4nI
plot([3.725 3.875],[3.8 3.2],'-','color',grey,'linewidth',3);

text(1, 2.5,'Proxy data SST:', 'fontname',fontnm, 'fontsize',fontsz)
plot(3.8,2.5,'.k', 'markersize',40)

text(1, 1.5,'Magnitude of change in data:', 'fontname',fontnm, 'fontsize',fontsz)
shade = fill([3.725 3.875 3.875 3.725],[1.0 1.6 2.0 1.4],lgrey,'edgecolor','none');
plot([3.725 3.875],[1.2 1.8],'-k')

text(1, 0.5,'No change pre- to post-EOT:', 'fontname',fontnm, 'fontsize',fontsz)
plot([3.725 3.875],[0.2 0.8],':k')

% text(3, 1,'Open / Closed Drake Passage', 'fontname',fontnm, 'fontsize',fontsz)
set(gca,'xlim',[0.75 7],'ylim',[0 5],'xtick',[],'ytick',[])
% axis off
box on

plot([4.25 4.25],[0.5 4.5],'-k')

set(gcf, 'color', 'w');

