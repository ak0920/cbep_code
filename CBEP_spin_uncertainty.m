% spin_uncertainty
% 
% This script calculates the uncertainty spread of Paul's long spin up runs
% 
% First caclualte the standard error (SEx = stdev/sqrt(n)) then multiply
% that by t value based upon the degrees of freedom (n-1)
% 
% Look up table used: http://www.itl.nist.gov/div898/handbook/eda/section3/eda3672.htm


%% Load data
% Long spin up simulation data
CBEP_spin_analysis

% Load gridcell size averages
proc_latlon_area

areas_frac = rot90(areas_frac);
areas_abs = rot90(areas_abs);
antarea = sum(sum(areas_abs(67:73,:)));


%% Calculate �95% uncertainty on the mean
% Find n (no. of pairs)
n=4; % for all pairs

% Ice changes (spatial)

% Calculate the standard error
icemidSEx = std(SATicemid,0,3)/sqrt(n);
iceendSEx = std(SATiceend,0,3)/sqrt(n);
% Calculate the �95% certainty bounds, with n-1 degrees of freedom
icemiduncertainty = icemidSEx * 3.182;
iceenduncertainty = iceendSEx * 3.182;


% CO2 changes (spatial)

% Calculate the standard error
CO2midSEx = std(SATCO2mid,0,3)/sqrt(n);
CO2endSEx = std(SATCO2end,0,3)/sqrt(n);
% Calculate the �95% certainty bounds, with n-1 degrees of freedom
CO2miduncertainty = CO2midSEx * 3.182;
CO2enduncertainty = CO2endSEx * 3.182;


% DP changes (spatial)

% Calculate the standard error
DPmidSEx = std(SATDPmid,0,3)/sqrt(n);
DPendSEx = std(SATDPend,0,3)/sqrt(n);
% Calculate the �95% certainty bounds, with n-1 degrees of freedom
DPmiduncertainty = DPmidSEx * 3.182;
DPenduncertainty = DPendSEx * 3.182;
