% CBEP_SATuncertainty
% 
% This script calculates the spatial uncertainty for each forcing (AIS
% growth, palaeogeographic change and pCO2 drop)
% 

% Load the mean spatial change data
CBEP_SATmeandif


%% Calculate the spatial �95% uncertainty

% First caclualte the standard error (SEx = stdev/sqrt(n)) then multiply
% that by t value based upon the degrees of freedom (n-1)
% 
% Look up table used: http://www.itl.nist.gov/div898/handbook/eda/section3/eda3672.htm


%% ice �95%

% Find n (no. of pairs)
icen = length(SATannicedif(1,1,:)); %9
% Calculate the standard error
iceSEx = SATstdicedif/sqrt(icen);
% Calculate the �95% certainty bounds, with n-1 degrees of freedom
iceuncertainty = iceSEx * 2.306;


%% geog �95% 

geogn = length(SATanngeogdif(1,1,:)); %9
geogSEx = SATstdgeogdif/sqrt(geogn);
geoguncertainty = geogSEx * 2.306;


%% CO2 �95% 

CO2n = length(SATannCO2dif(1,1,:)); %7
CO2SEx = SATstdCO2dif/sqrt(CO2n);
CO2uncertainty = CO2SEx * 2.447;
