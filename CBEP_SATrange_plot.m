% CBEP_SATuncertainty_plot
% 
% This script makes a plot of the mean spatial change due to AIS growth,
% palaeogeographic change and CO2 drop, as well the ensemble mean global
% annual SAT response (Supp. Figure 3). 
% 

% Load the mean spatial change data
CBEP_SATmeandif

% Load the lat-lon grids for using the mapping toolbox
if strcmp(pwd,'/Users/ak0920/Data/EO_more')
    run('../EO_code/load_latlon.m')
else load_latlon
end


%% Setup some figure properties
close(gcf)
figure

load('rangecol.mat')
colormap(rangecol)
caxis([-40 40])
font = 'arial';
fontsz = 16;
set(gcf, 'color', 'w');
latlim = [-90 90]; 
lonlim = [0 356.25];



%% Plot ice �95%

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(abs(max(SATannicedif,[],3)-min(SATannicedif,[],3))),[0:1:10], 'LineStyle', 'none') % max and min in 3rd dimension (i.e. for all simulation pairs for each forcing)

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis([0 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['AIS growth: n=',num2str(length(SATannicedif(1,1,:)))])

hold on

% Add LSMs, with oldest in palest grey, most recent in black
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Plot geog �95% 

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(abs(max(SATanngeogdif,[],3)-min(SATanngeogdif,[],3))),[0:1:10], 'LineStyle', 'none')

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis([0 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['Palaeogeographic change: n=',num2str(length(SATanngeogdif(1,1,:)))])

hold on

% Add LSMs
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Plot CO2 �95% 

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(abs(max(SATannCO2dif,[],3)-min(SATannCO2dif,[],3))),[0:1:10], 'LineStyle', 'none') 

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis([0 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['CO_2 drop: n=',num2str(length(SATannCO2dif(1,1,:)))])

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change range (�C)', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [0 10], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz) % this range is probably bigger than is necessary

hold on

% Add LSMs
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Final tidy up
% Resize the figure
x0=10;
y0=10;
width=600;
height=1200;
set(gcf,'units','points','position',[x0,y0,width,height])% set(gcf, 'color', 'w');
