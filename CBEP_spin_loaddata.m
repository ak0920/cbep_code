% CBEP_spin_loaddata
%
% Load Paul's simulations and calculate means for the time periods 1000-1070
% and (t-100)-(t) where t is the final year of the simulation
%
% This a ~slow script, so it loads already processed data.
%
% File naming convention:
% [VAR]spin.[spin state][CO2][ice sheet state][DP STATE]
%     i.e. mid/end 2/3 n/f O(pen)/C(losed)
%
% Monthly file format:
% 4D: log, lat, depth (usually 1) and time

% If the script has run before, load the processed/saved data
if exist('SATspin.mat','file')
    load('SATspin.mat')
    
% Otherwise load and process it from scratch:    
else
    % Define the averaging periods
    end100 = 12001:13200;
    % Select the desired averaging period (I used to have multiple options here)
    aveperiod = end100;
    
    % List the model simulations
    models = {'tecqn','tecqn1', 'tecqo','tecqo2','tecqp','tecqp2','tecqq','tecqq1',...
        'tecqs','tecqs1','tecqv','tecqv1','tecqt','tecqt3','tecqu','tecqu2'};
    
    % List the simulation descriptors
    desc = {'2fO','2fO','2fC','2fC','2nC','2nC','2nO','2nO','3fO','3fO','3nO','3nO','3fC','3fC','3nC','3nC'};
    
    for i = 1:length(models)
        
        % Convert cells into useable string
        model_cell = models(i);
        model_str = sprintf('%s',model_cell{:});
        
        % Load the raw monthly data
        SATmon = ncread(['../OtherEO/',model_str,'.temp_mm_1_5m.monthly.nc'],'temp_mm_1_5m');
        
        % If the simulation is mid-spin-up, use the selected averaging period
        if length(model_str) == 5
            desc_cell = desc(i);
            desc_str = ['mid',sprintf('%s',desc_cell{:})];
            SATspin.(desc_str) = central_gm(flipud(rot90(mean(SATmon(:,:,1,aveperiod),4))))-273.15;
            
        % Otherwise take the average over the final 100 years
        else
            desc_cell = desc(i);
            desc_str = ['end',sprintf('%s',desc_cell{:})];
            t = length(SATmon(1,1,1,:));
            SATspin.(desc_str) = central_gm(flipud(rot90(mean(SATmon(:,:,1,t-1199:t),4))))-273.15;
        end
        
    end
    
    % Tidy up and save the data to save time later
    clear SATmon t i desc desc_str desc_cell models model_str model_cell
    save('SATspin.mat', 'SATspin');
    
end


% Load LSM
LSMspin = ~isnan(rot90(central_gm(ncread('../OtherEO/tecqqo.pfclann.nc','temp_mm_uo'))));
