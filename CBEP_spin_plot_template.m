% CBEP_spin_plot_template
% 
% This script does main plotting for spin-up simulations figures
% 
% It requires data and colrange to be defined before running
% 

% Some default values for all plots
font = 'arial';
fontsz = 16;
set(gcf, 'color', 'w');
latlim = [-90 90]; 
lonlim = [0 356.25];

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(data),colrange, 'LineStyle', 'none')

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis([min(colrange) max(colrange)])
set(gca,'fontname',font,'fontsize',fontsz)

hold on

% Add LSM
contourm(lat,lon,double(flipud(LSMspin)),[0 0],'k','linewidth',1.5)
