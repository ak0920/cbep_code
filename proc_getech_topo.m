% proc_getech_topo
% 
% This script reads in the Getech geographies for glaciated/ice free
% stages, at both model and high res
% 

%% Load model res data
% For Rupelian
ruptopo_latlonL = ncread('../Getech/ancil/rup/rup_010.nc','oroguk');
rupbath_latlonL = ncread('../Getech/ancil/rup/rup_010.nc','bathuk');
rupicet_latlonL = ncread('../Getech/ancil/rup/rup_025.nc','oroguk');

% Sea in ruptopo is set to -10e5, which confuses ice thickness - make it 0
ruptopo_latlonL2 = ruptopo_latlonL;
ruptopo_latlonL2(ruptopo_latlonL<0) = 0;

% Find ice thickness
rupice_latlonL = rupicet_latlonL-ruptopo_latlonL2;

% Include bathymetry in topography
ruptopo_latlonL(rupbath_latlonL>0) = -rupbath_latlonL(rupbath_latlonL>0);

% For Rupelian
ruptopo_latlonL = ncread('../Getech/ancil/rup/rup_010.nc','oroguk');
rupbath_latlonL = ncread('../Getech/ancil/rup/rup_010.nc','bathuk');
rupicet_latlonL = ncread('../Getech/ancil/rup/rup_025.nc','oroguk');

% Sea in ruptopo is set to -10e5, which confuses ice thickness - make it 0
ruptopo_latlonL2 = ruptopo_latlonL;
ruptopo_latlonL2(ruptopo_latlonL<0) = 0;

% Find ice thickness
rupice_latlonL = rupicet_latlonL-ruptopo_latlonL2;

% Include bathymetry in topography
ruptopo_latlonL(rupbath_latlonL>0) = -rupbath_latlonL(rupbath_latlonL>0);