% CBEP_topo_plot
% 
% This script makes a plot of the palaeogeogs used in the CBEP paper (Supp.
% Figure 1).
% This script currently works on my local machine only
% 

% Load general data (for LSM)
CBEP_loaddata

% Load the lat-lon grids for using the mapping toolbox
if strcmp(pwd,'/Users/ak0920/Data/EO_more')
    run('../EO_code/load_latlon.m')
else load_latlon
end
lon_lsm = double(ncread('../Getech/ancil/rup/rup_010.nc', 'longitude'));
lat_lsm = double(ncread('../Getech/ancil/rup/rup_010.nc', 'latitude'));


% Load the Getech orography and bathymetry fields
oror    = rot90(ncread('../Getech/ancil/rup/rup_010.nc', 'oroguk'));
oroc    = rot90(ncread('../Getech/ancil/cha/cha_010.nc', 'oroguk'));
orop    = rot90(ncread('../Getech/ancil/pri/pri_010.nc', 'oroguk'));
batr    = rot90(ncread('../Getech/ancil/rup/rup_010.nc', 'bathuk'));
batc    = rot90(ncread('../Getech/ancil/cha/cha_010.nc', 'bathuk'));
batp    = rot90(ncread('../Getech/ancil/pri/pri_010.nc', 'bathuk'));

% Load the Robertsons orography and bathymetry fields
ororr   = rot90(ncread('../Getech/ancil/ruph/tecqv.qrparm.orog.nc', 'ht'));
batrr   = flipud(rot90(ncread('../Getech/ancil/ruph/tecqv.qrparm.omask.nc', 'depthdepth')));

% Merge the orography and bathymetry into one field
orobr = oror;
orobr(batr>0) = -batr(batr>0);
orobc = oroc;
orobc(batc>0) = -batc(batc>0);
orobp = orop;
orobp(batp>0) = -batp(batp>0);
orobrr = ororr;
orobrr(batrr>0) = -batrr(batrr>0);

% Centre the longitude to 0�W
orobr = central_gm(double(orobr)); 
orobc = central_gm(double(orobc)); 
orobp = central_gm(double(orobp)); 
orobrr = central_gm(double(orobrr)); 


%% Set some figure properties

% Load useful colours
load col_topo1.mat % aka colors2
load col_topo2.mat % aka colors3
load col_grey.mat
red = [0.850000000000000,0.0872549042105675,0];

close(gcf)
figure

colors = colors2;
colrange = [-6000 6000];
font = 'arial';
fontsz = 16;
set(gcf, 'color', 'w');
latlim = [-90 90]; 
lonlim = [0 356.25];



%% Plot Priabonian
% Set plot position
subplot('position', [0.2 0.765 0.5 0.2])

% Plot the global map
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobp))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)
title('Priabonian (Getech)')

hold on

% Add LSM (shaded differently for each Stage)
contourm(lat,lon+1.875,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)

% Plot proxy locations (only for this Stage)
proxy_lons = [21,86,50,1,93,2,86,10,75];
proxy_lats = [13,16,13,18,61,65,38,31,49];
plotm(lat(proxy_lats),lon(proxy_lons)-180,'p','markeredgeColor','k','markerFaceColor',red,'markersize',8)

% Plot the Antarctic polar projection
subplot('position', [0.7 0.765 0.2 0.2])
axesm('MapProjection','stereo', 'MapLatLim', [-90 -60],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobp))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)

hold on

% Add LSM (shaded differently for each Stage)
contourm(lat+2.5,lon+1.875,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)

% Highlight Gamburtsev area
plotm(lat(73-[66,68,68,66,66,66]),lon([67,67,73,73,70,67]),'-','color',red,'linewidth',1.5)


%% Plot Rupelian
% Set plot position
subplot('position', [0.2 0.515 0.5 0.2])

% Plot global map
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobr))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)
title('Rupelian (Getech)')

hold on

% Add LSM
contourm(lat,lon+1.875,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)

% Antarctic polar projection plot
subplot('position', [0.7 0.515 0.2 0.2])
axesm('MapProjection','stereo', 'MapLatLim', [-90 -60],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobr))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)

hold on

% Add LSM
contourm(lat+2.5,lon+1.875,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)

% Highlight Gamburtsev area
plotm(lat(73-[66,68,68,66,66,66]),lon([67,67,73,73,70,67]),'-','color',red,'linewidth',1.5)


%% Plot Chattian
% Set plot position
subplot('position', [0.2 0.265 0.5 0.2])

% Plot global map
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobc))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)
title('Chattian (Getech)')

hold on

% Add LSMs
contourm(lat,lon+1.875,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)

% Antarctic polar projection plot
subplot('position', [0.7 0.265 0.2 0.2])
axesm('MapProjection','stereo', 'MapLatLim', [-90 -60],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobc))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)

hold on

% Add LSM
contourm(lat+2.5,lon+1.875,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)

% Highlight Gamburtsev area
plotm(lat(73-[66,68,68,66,66,66]),lon([67,67,73,73,70,67]),'-','color',red,'linewidth',1.5)



%% Plot Rupelian Robertsons
% Set plot position
subplot('position', [0.2 0.015 0.5 0.2])

% Plot global map
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... % eqdcylin    Robinson
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobrr))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)
title('Rupelian alt. (Robertsons)')

hold on

% Add LSMs
contourm(lat,lon+1.875,double(flipud(LSM.end2nO)),[0 0],'linecolor','k','linewidth',1.5)

% Antarctic polar projection plot
subplot('position', [0.7 0.015 0.2 0.2])
axesm('MapProjection','stereo', 'MapLatLim', [-90 -60],...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
pcolorm(lat_lsm,lon_lsm,flipud(orobrr))

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(colors)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis(colrange)
set(gca,'fontname',font,'fontsize',fontsz)

hold on

% Add LSM
contourm(lat+2.5,lon+1.875,double(flipud(LSM.end2nO)),[0 0],'linecolor','k','linewidth',1.5)

% Highlight Gamburtsev area
plotm(lat(73-[66,68,68,66,66,66]),lon([67,67,73,73,70,67]),'-','color',red,'linewidth',1.5)


%% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.15 0.25 0.03 0.5]);
ylabel(color, 'Elevation (m a.s.l.)', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', colrange, 'YAxisLocation','left', 'fontname', font, 'fontsize', fontsz)

%% Final tidy up
% Resize the figure
x0=20;
y0=10;
width=600;
height=1200;
set(gcf,'units','points','position',[x0,y0,width,height])% set(gcf, 'color', 'w');
