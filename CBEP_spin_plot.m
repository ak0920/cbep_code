% CBEP_spin_plot
% 
% This script does all of the plots relating to the spin-up simulations,
% including the mean responses mid-way through and at the end of the
% spin-up, the magnitude of change between the mid-point and the end of the
% spin-up and the �95% uncertainty mid-way through and at the end of the
% spin-up.
% 
% It should have been done more neatly, rather than copy-pasted so many
% times, but I was being lazy (and adding to it gradually).
% 

% Load the mean spatial change data
CBEP_spin_analysis % Calculates the mean responses
CBEP_spin_uncertainty % Calculates the uncertainty on the mean responses

% Load the lat-lon grids for using the mapping toolbox
if strcmp(pwd,'/Users/ak0920/Data/EO_more')
    run('../EO_code/load_latlon.m')
else load_latlon
end

% Load all of the colour scales
load('RBcolgradcon.mat')
load('uncertaintycol.mat')
load('differencecol.mat')

% For resizing figures
x0=10;
y0=10;
width=600;
height=1200;

%% Mean responses mid-way through spin-up: Setup some figure properties
close all
figure
set(gcf,'units','points','position',[x0,y0,width,height])

colormap(RBcolgradcon)
colrange = -10:10;

%% Plot mean AIS growth response (mid-way through)

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot using template
data = SATicemidmean;
CBEP_spin_plot_template

% Add extras
title('AIS growth: n=4')

% Add stippling where n of the models agree on direction of change
stiparea = sum(SATicemid>0,3) == length(SATicemid(1,1,:)) | sum(SATicemid>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end


%% Plot mean DP opening response (mid-way through) 

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot using template
data = SATDPmidmean;
CBEP_spin_plot_template

% Add extras
title('DP change: n=4')

% Add stippling where n of the models agree on direction of change
stiparea = sum(SATDPmid>0,3) == length(SATDPmid(1,1,:)) | sum(SATDPmid>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end


%% Plot mean CO2 drop response (mid-way through)

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot using template
data = SATCO2midmean;
CBEP_spin_plot_template

% Add extras
title('CO_2 drop: n=4')

% Add stippling where n of the models agree on direction of change
stiparea = sum(SATCO2mid>0,3) == length(SATCO2mid(1,1,:)) | sum(SATCO2mid>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change (�C) [after 1000 years]', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [-10 10], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz) 


%% Mean responses at end of spin-up: Setup some figure properties
figure
set(gcf,'units','points','position',[x0,y0,width,height])

colormap(RBcolgradcon)
colrange = -10:10;

%% Plot mean AIS growth response (end of spin-up)

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot using template
data = SATiceendmean;
CBEP_spin_plot_template

% Add extras
title('AIS growth: n=4')

% Add stippling where n-1 of the models agree on direction of change
stiparea = sum(SATiceend>0,3) == length(SATiceend(1,1,:)) | sum(SATiceend>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end


%% Plot mean DP opening response (end of spin-up) 

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot using template
data = SATDPendmean;
CBEP_spin_plot_template

% Add extras
title('DP change: n=4')

% Add stippling where n of the models agree on direction of change
stiparea = sum(SATDPend>0,3) == length(SATDPend(1,1,:)) | sum(SATDPend>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end


%% Plot mean CO2 drop response (end of spin-up)

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot using template
data = SATCO2endmean;
CBEP_spin_plot_template

% Add extras
title('CO_2 drop: n=4')

% Add stippling where n of the models agree on direction of change
stiparea = sum(SATCO2end>0,3) == length(SATCO2end(1,1,:)) | sum(SATCO2end>0,3) == 0;

% Add the stippling
% (Add a dot at every other gridcell to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            % (plot the dots, centred on each gridcell with size scaled to
            % latitude, getting smaller nearer the poles to reduce clutter)
            plotm(-lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',6-abs(lat(i))/30)
        end
    end
end

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change (�C) [end of spin-up]', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [-10 10], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz) 


%% Error/change through spin-up: Setup some figure properties
figure
set(gcf,'units','points','position',[x0,y0,width,height])

colormap(differencecol)
colrange = 0:0.5:5;


%% Plot AIS growth spin-up error/change

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot using template
data = abs(SATiceendmean-SATicemidmean);
CBEP_spin_plot_template

% Add extras
title('AIS growth: n=4')

%% Plot DP opening spin-up error/change

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot using template
data = abs(SATDPendmean-SATDPmidmean);
CBEP_spin_plot_template

% Add extras
title('DP change: n=4')


%% Plot CO2 drop spin-up error/change

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot using template
data = abs(SATCO2endmean-SATCO2midmean);
CBEP_spin_plot_template

% Add extras
title('CO_2 drop: n=4')

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Magnitude of annual mean SAT change through spin-up (�C)', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [0 5], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz) 


%% Mean response uncertainty (mid-way through): Setup some figure properties
figure
set(gcf,'units','points','position',[x0,y0,width,height])

colormap(uncertaintycol)
colrange = 0:0.5:5;


%% Plot AIS growth response uncertainty (mid-way through)

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot using template
data = icemiduncertainty;
CBEP_spin_plot_template

% Add extras
title('AIS growth: n=4')


%% Plot DP opening response uncertainty (mid-way through)

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot using template
data = DPmiduncertainty;
CBEP_spin_plot_template

% Add extras
title('DP change: n=4')


%% Plot CO2 drop response uncertainty (mid-way through)

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot using template
data = CO2miduncertainty;
CBEP_spin_plot_template

% Add extras
title('CO_2 drop: n=4')

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change �95% uncertainty (�C) [after 1000 years]', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [0 5], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz)


%% Mean response uncertainty (end of spin-up): Setup some figure properties
figure
set(gcf,'units','points','position',[x0,y0,width,height])

colormap(uncertaintycol)
colrange = 0:0.5:5;

%% Plot AIS growth response uncertainty (end of spin-up)

% Set plot position
subplot('position', [0.05 0.6875 0.8 0.275])

% Plot using template
data = iceenduncertainty;
CBEP_spin_plot_template

% Add extras
title('AIS growth: n=4')


%% Plot DP opening response uncertainty (end of spin-up)

% Set plot position
subplot('position', [0.05 0.3675 0.8 0.275])

% Plot using template
data = DPenduncertainty;
CBEP_spin_plot_template

% Add extras
title('DP change: n=4')


%% Plot CO2 drop response uncertainty (end of spin-up)

% Set plot position
subplot('position', [0.05 0.0475 0.8 0.275])

% Plot using template
data = CO2enduncertainty;
CBEP_spin_plot_template

% Add extras
title('CO_2 drop: n=4')

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.85 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change �95% uncertainty (�C) [end of spin-up]', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [0 5], 'YAxisLocation','right', 'fontname', font, 'fontsize', fontsz) 
