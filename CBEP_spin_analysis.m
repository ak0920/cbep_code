% CBEP_spin_analysis
% 
% Find the mean difference for each forcing (ice, DP. CO2) on the SATspin
% simulations
% 

%% Load data
CBEP_spin_loaddata

%% Find ice differences

% Mid-way through spin-up

% Store as a structure to access later (if necessary)
SATice.mid2O = SATspin.mid2fO - SATspin.mid2nO;
SATice.mid3O = SATspin.mid3fO - SATspin.mid3nO;
SATice.mid2C = SATspin.mid2fC - SATspin.mid2nC;
SATice.mid3C = SATspin.mid3fC - SATspin.mid3nC;

% Also store in single array for quicker analysis
SATicemid = cat(3,SATice.mid2O,SATice.mid3O,SATice.mid2C,SATice.mid3C);

% Find average
SATicemidmean = mean(SATicemid,3);

% End of spin-up

% Store as a structure to access later (if necessary)
SATice.end2O = SATspin.end2fO - SATspin.end2nO;
SATice.end3O = SATspin.end3fO - SATspin.end3nO;
SATice.end2C = SATspin.end2fC - SATspin.end2nC;
SATice.end3C = SATspin.end3fC - SATspin.end3nC;

% Also store in single array for quicker analysis
SATiceend = cat(3,SATice.end2O,SATice.end3O,SATice.end2C,SATice.end3C);

% Find average
SATiceendmean = mean(SATiceend,3);

%% Find CO2 differences
% Mid-way through spin-up
SATCO2.midfO = SATspin.mid2fO - SATspin.mid3fO;
SATCO2.midnO = SATspin.mid2nO - SATspin.mid3nO;
SATCO2.midfC = SATspin.mid2fC - SATspin.mid3fC;
SATCO2.midnC = SATspin.mid2nC - SATspin.mid3nC;

SATCO2mid = cat(3,SATCO2.midfO,SATCO2.midnO,SATCO2.midfC,SATCO2.midnC);
SATCO2midmean = mean(SATCO2mid,3);

% End of spin-up
SATCO2.endfO = SATspin.end2fO - SATspin.end3fO;
SATCO2.endnO = SATspin.end2nO - SATspin.end3nO;
SATCO2.endfC = SATspin.end2fC - SATspin.end3fC;
SATCO2.endnC = SATspin.end2nC - SATspin.end3nC;

SATCO2end = cat(3,SATCO2.endfO,SATCO2.endnO,SATCO2.endfC,SATCO2.endnC);
SATCO2endmean = mean(SATCO2end,3);

%% Find DP differences
% Mid-way through spin-up
SATDP.mid2f = SATspin.mid2fO - SATspin.mid2fC;
SATDP.mid2n = SATspin.mid2nO - SATspin.mid2nC;
SATDP.mid3f = SATspin.mid3fO - SATspin.mid3fC;
SATDP.mid3n = SATspin.mid3nO - SATspin.mid3nC;

SATDPmid = cat(3,SATDP.mid2f,SATDP.mid2n,SATDP.mid3f,SATDP.mid3n);
SATDPmidmean = mean(SATDPmid,3);

% End of spin-up
SATDP.end2f = SATspin.end2fO - SATspin.end2fC;
SATDP.end2n = SATspin.end2nO - SATspin.end2nC;
SATDP.end3f = SATspin.end3fO - SATspin.end3fC;
SATDP.end3n = SATspin.end3nO - SATspin.end3nC;

SATDPend = cat(3,SATDP.end2f,SATDP.end2n,SATDP.end3f,SATDP.end3n);
SATDPendmean = mean(SATDPend,3);
