% CBEP_check_lapse
% 
% Find out the approximate contribution of lapse rate vs albedo for cooling
% over Antarctica due to glaciation
% 

%% Load some data

CBEP_loaddata
CBEP_check_albedo
proc_latlon_area

% Define some constants/variables
solcha = 1362.00;
solrup = 1361.35;
solpri = 1360.86;
SBcon = 5.7e-8;

models = {'tdluy','tdlux', 'tdzse','tdluu','tdluq','tdwqf','tdluv','tdluw',...
    'tdlut','tdwqe','tdlup','tdwqk','tdwqv','tdzsc','tdzsd',...
    'tecqn1','tecqo2','tecqq1','tecqp2','tecqs1','tecqt3','tecqv1','tecqu2'};

% More useful descriptions of the simulations
desc = {'cha4nI','cha4eI', 'cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
    'rup2nI','rup2eI','rup2fI','end3fO','end3nO','end2fO','end2nO','end3fC','end3nC','end2fC','end2nC',...
    'pri4nI','pri4fI','pri2nI','pri2fI'};

% Load the data
for i = 1:length(models)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    model_cell = models(i);
    model_str = sprintf('%s',model_cell{:}); % this is used to find the model folder on BRIDGE machines, including the simulation cont. number, e.g. tecqt3
    model_str2 = model_str(1:5); % this is used for the .nc files which do not include the simulation cont. number e.g. tecqt
    
    % Find if running locally or on eocene
    if strcmp(pwd,'/Users/ak0920/Data/EO_more')
        if strcmp(desc_str(1:3),'end')
            datadir = '../OtherEO/'; % Paul's runs stored here
        else
            datadir = '../EO_data/'; % Getech runs stored here

        end
    else
        datadir = ['/home/bridge/ak0920/ummodel/data/',model_str,'/climate/']; 

    end
    
    % Make string names for the various netCDFs
    netcdf1 = [datadir,model_str2,'a.pdclann.nc'];

    % Load SST and add to structure

    GlobSAT.(desc_str) = sum(sum(rot90(areas_frac).*SAT.(desc_str)));
    GlobSAT2(i) = sum(sum(rot90(areas_frac).*SAT.(desc_str)));
    
    GlobSST.(desc_str) = nansum(nansum( (rot90(areas_abs).*SST.(desc_str))/nansum(nansum(areas_abs(~isnan(rot90(SST.(desc_str)))))) ));
    GlobSST2(i) = GlobSST.(desc_str);
    
    SICtemp =  rot90(areas_abs).*SIC.(desc_str);
    AntSIC.(desc_str) = nansum(nansum(SICtemp(37:73,:)));
    AntSIC2(i) = nansum(nansum(SICtemp(37:73,:)));
    
    RadTOA.(desc_str) = nansum(nansum(rot90(areas_frac).*(SWin.(desc_str) - SWout.(desc_str) - LWout.(desc_str))));
    RadTOA2(i) = RadTOA.(desc_str);
    
%     % Make string names for the various netCDFs
%     netcdf1 = [datadir,model_str2,'o.pfclann.nc'];
%     netcdf2 = [datadir,model_str2,'a.pdclann.nc'];
%     netcdf3 = [datadir,model_str2,'a.pdcldjf.nc'];
%     netcdf3b = [datadir,model_str2,'a.pdcljja.nc'];
%     netcdf4 = [datadir,model_str2,'a.pdsdann.nc'];
%     netcdf5 = [datadir,model_str2,'o.meridclann.nc'];
%     netcdf6 = [datadir,model_str2,'o.pgclann.nc'];
%     
% 
%     % Load SST and add to structure
%     SST.(desc_str) = rot90(central_gm(ncread(netcdf1, 'temp_mm_uo'))); % units = �C
%     SAL.(desc_str) = rot90(central_gm(ncread(netcdf1, 'salinity_mm_dpth'))); % units = �C
%    
%     % Load SAT and its standard deviation (the if statement is required
%     % locally as I don't have all of the sd files on my machine)
%     SAT.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
%     if exist(netcdf4,'file');
%         SATstd.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf4, 'temp_mm_1_5m')))); % units = �C
%     end
%     
%     % Load precipitation
%     P.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'precip_mm_srf'))))*3600*24*365; % convert mm/s -> mm/a
%     
%     % Load cloud cover
%     cld.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'totCloud_mm_ua')))); % units = fractional coverage
%     
%     % Load net long and short wave radiation
%     LWR.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'longwave_mm_s3_srf')))); % units = W/m2
%     SWR.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'solar_mm_s3_srf')))); % units = W/m2
%     
%     % Load sea ice
%     SIC.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'iceconc_mm_srf')))); % units = fractional coverage
%     
%     % Load Antarctic summer/winter SAT
%     SATdjf.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf3, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
%     SATjja.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf3b, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    
end


%% Find differences between
% Identifiers for glaciated icehouse simulations with a unglaciated pair
desc = {'cha4eI','cha4fI','rup4fI','cha2fI','cha2eI','rup2fI','rup2eI','pri4fI','pri2fI'};%,'end3fO','end2fO','end3fC','end2fC'};

% Make empty array to store each ice comparison
% GlobSATicedif = nan(73,96,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (glaciated identifiers) into useable string
    desc_cell = desc(i);
    desc_ice = sprintf('%s',desc_cell{:});
    
    % Find unglaciated identifiers
    desc_noice = [desc_ice(1:4),'n',desc_ice(6)];

    % Find difference
    GlobSATicedif(i) = GlobSAT.(desc_ice) - GlobSAT.(desc_noice);
    if desc_ice(1) == 'c'
        albedoicedif(i) = ((solcha*(1-albedo.(desc_ice)))/(4*SBcon))^0.25-((solcha*(1-albedo.(desc_noice)))/(4*SBcon))^0.25;
    else if desc_ice(1) == 'r'
            albedoicedif(i) = ((solrup*(1-albedo.(desc_ice)))/(4*SBcon))^0.25-((solrup*(1-albedo.(desc_noice)))/(4*SBcon))^0.25;
        else
            albedoicedif(i) = ((solpri*(1-albedo.(desc_ice)))/(4*SBcon))^0.25-((solpri*(1-albedo.(desc_noice)))/(4*SBcon))^0.25;
        end
    end

    
end

% Find mean of all pairs
GlobSATmeanicedif = mean(GlobSATicedif);
% Find standard deviation of all pairs
SATstdicedif = std(GlobSATicedif);



% tempdif = sum(sum(rot90(areas_frac).*SAT.cha4fI))-sum(sum(rot90(areas_frac).*SAT.cha4nI));
% albdif = ((solcha*(1-albedo.cha4fI))/(4*SBcon))^0.25-((solcha*(1-albedo.cha4nI))/(4*SBcon))^0.25;
