% CBEP_loaddata
% 
% This script loads all of the Getech ensemble data for simulations that are correct
% as well as Paul's long spin-up simulations. 
% The format of the data is:
%    VARIABLE.[stage][CO2][ice sheet][ISLAND]
% where:
%    stage = first 3 letters of stage name or 'end' for Paul's
%    CO2 = 4, 3 or 2 (x pre-industrial levels)
%    ice sheet = n (no ice), e (EAIS) or f (full AIS)
%    ISLAND = I (Getech only: Antarctica = island), O (Paul's: DP = open), 
%       C (Paul's: DP = closed) 

% List of simulation names
models = {'tdluy','tdlux', 'tdzse','tdluu','tdluq','tdwqf','tdluv','tdluw',...
    'tdlut','tdwqe','tdlup','tdwqk','tdwqv','tdzsc','tdzsd',...
    'tecqn1','tecqo2','tecqq1','tecqp2','tecqs1','tecqt3','tecqv1','tecqu2'};

% More useful descriptions of the simulations
desc = {'cha4nI','cha4eI', 'cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
    'rup2nI','rup2eI','rup2fI','pri2nI','pri2fI','pri4nI','pri4fI',...
    'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Load the data
for i = 1:length(models)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    model_cell = models(i);
    model_str = sprintf('%s',model_cell{:}); % this is used to find the model folder on BRIDGE machines, including the simulation cont. number, e.g. tecqt3
    model_str2 = model_str(1:5); % this is used for the .nc files which do not include the simulation cont. number e.g. tecqt
    
    % Find if running locally or on eocene
    if strcmp(pwd,'/Users/ak0920/Data/CBEP_code')
        if strcmp(desc_str(1:3),'end')
            datadir = '../OtherEO/'; % Paul's runs stored here
        else
            datadir = '../EO_data/'; % Getech runs stored here

        end
    else
        datadir = ['/home/bridge/ak0920/ummodel/data/',model_str,'/climate/']; 

    end
    
    % Make string names for the various netCDFs
    netcdf1 = [datadir,model_str2,'o.pfclann.nc'];
    netcdf2 = [datadir,model_str2,'a.pdclann.nc'];
    netcdf3 = [datadir,model_str2,'a.pdcldjf.nc'];
    netcdf3b = [datadir,model_str2,'a.pdcljja.nc'];
    netcdf4 = [datadir,model_str2,'a.pdsdann.nc'];
    netcdf5 = [datadir,model_str2,'o.meridclann.nc'];
    netcdf6 = [datadir,model_str2,'o.pgclann.nc'];
    

    % Load SST and add to structure
    SST.(desc_str) = rot90(central_gm(ncread(netcdf1, 'temp_mm_uo'))); % units = �C
    SAL.(desc_str) = rot90(central_gm(ncread(netcdf1, 'salinity_mm_dpth'))); % units = �C
   
    % Load SAT and its standard deviation (the if statement is required
    % locally as I don't have all of the sd files on my machine)
    SAT.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    if exist(netcdf4,'file');
        SATstd.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf4, 'temp_mm_1_5m')))); % units = �C
    end
    
    % Load precipitation
    P.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'precip_mm_srf'))))*3600*24*365; % convert mm/s -> mm/a
    
    % Load cloud cover
    cld.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'totCloud_mm_ua')))); % units = fractional coverage
    
    % Load net long and short wave radiation
    LWR.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'longwave_mm_s3_srf')))); % units = W/m2
    SWR.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'solar_mm_s3_srf')))); % units = W/m2
    
    % Load sea ice
    SIC.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf3b, 'iceconc_mm_srf')))); % units = fractional coverage
    
    % Load Antarctic summer/winter SAT
    SATdjf.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf3, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    SATjja.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf3b, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    
    % Load MLD and streamfunction data
    MLD.(desc_str) = rot90(central_gm(ncread(netcdf1, 'mixLyrDpth_mm_uo'))); % units = m
    STR.(desc_str) = rot90(central_gm(ncread(netcdf1, 'streamFn_mm_uo'))/1e12); % convert cm3/s -> Sv
    
    % Load meridional stream function
    mSTR.(desc_str) = flipud(rot90(ncread(netcdf5, 'Merid_Global'))); % units = Sv
    
    % Generate LSM  
    LSM.(desc_str) = ~isnan(rot90(central_gm(ncread(netcdf1,'temp_mm_uo'))));
    
    % Load all 3D ocean temperature data
    if exist(netcdf6,'file');
        OceanT.(desc_str) = ncread(netcdf6, 'temp_ym_dpth'); 
    end

end