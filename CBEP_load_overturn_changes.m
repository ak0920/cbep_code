% CBEP_load_overturn_changes
% 
% This calculates mean changes in overturning and MLD variables with 
% changing ice sheet, palaeogeog and CO2 from the Getech simulations (not Paul's)
% 

%% Load data and any prep work
CBEP_load_overturn_values

% Load gridcell size averages
proc_latlon_area
areas_frac = rot90(areas_frac);
areas_abs = rot90(areas_abs);


%% Mean changes in response to AIS growth
desc = {'cha4eI','cha4fI','rup4fI','cha2fI','cha2eI','rup2fI','rup2eI','pri4fI','pri2fI'};

% Make empty arrays for global means:
icemean_SMOC = zeros(length(desc),1); % Gamburtsev summer mean SAT
icemean_NMOC = zeros(length(desc),1); % Southern Ocean SST
icemean_NAMLD = zeros(length(desc),1); % Max N. Atlantic MLD
icemean_SOMLD = zeros(length(desc),1); % Antarctic mean P

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (glaciated identifiers) into useable string
    desc_cell = desc(i);
    desc_ice = sprintf('%s',desc_cell{:});
    
    % Find unglaciated identifiers
    desc_noice = [desc_ice(1:4),'n',desc_ice(6)];
    
    % Find change
    icemean_SMOC(i) = S_MOC2.(desc_ice) - S_MOC2.(desc_noice);
    icemean_NMOC(i) = N_MOC2.(desc_ice) - N_MOC2.(desc_noice);
    icemean_NAMLD(i) = NAtl_MLD2.(desc_ice) - NAtl_MLD2.(desc_noice);
    icemean_SOMLD(i) = SO_MLD2.(desc_ice) - SO_MLD2.(desc_noice);

end

% Calculate �95% uncertainty on the mean
% Find n (no. of pairs)
icen = length(icemean_SOMLD); %9

% Calculate the standard error
iceSEx_SMOC = std(icemean_SMOC)/sqrt(icen);
iceSEx_NMOC = std(icemean_NMOC)/sqrt(icen);
iceSEx_SOMLD = std(icemean_SOMLD)/sqrt(icen);
iceSEx_NAMLD = std(icemean_NAMLD)/sqrt(icen);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
iceuncertainty_SMOC = iceSEx_SMOC * 2.306;
iceuncertainty_NMOC = iceSEx_NMOC * 2.306;
iceuncertainty_SOMLD = iceSEx_SOMLD * 2.306;
iceuncertainty_NAMLD = iceSEx_NAMLD * 2.306;

%% Mean changes in response to paleogeog change
desc = {'cha4nI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI','rup2nI','rup2fI'};

geogmean_SMOC = zeros(length(desc),1); 
geogmean_NMOC = zeros(length(desc),1); 
geogmean_NAMLD = zeros(length(desc),1); 
geogmean_SOMLD = zeros(length(desc),1);

for i = 1:length(desc)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Find previous equivalent palaeogeog
    if desc_str(1) == 'c'
        descdif = ['rup',desc_str(4:6)];
    else if desc_str(1) == 'r'
            descdif = ['pri',desc_str(4:6)];
        end
    end
    
    % Find change
    geogmean_SMOC(i) = S_MOC2.(desc_str) - S_MOC2.(descdif);
    geogmean_NMOC(i) = N_MOC2.(desc_str) - N_MOC2.(descdif);
    geogmean_NAMLD(i) = NAtl_MLD2.(desc_str) - NAtl_MLD2.(descdif);
    geogmean_SOMLD(i) = SO_MLD2.(desc_str) - SO_MLD2.(descdif);

end

% Calculate �95% uncertainty of the mean
geogn = length(geogmean_SOMLD); %9

% Calculate the standard error
geogSEx_SMOC = std(geogmean_SMOC)/sqrt(icen);
geogSEx_NMOC = std(geogmean_NMOC)/sqrt(icen);
geogSEx_NAMLD = std(geogmean_NAMLD)/sqrt(icen);
geogSEx_SOMLD = std(geogmean_SOMLD)/sqrt(icen);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
geoguncertainty_SMOC = geogSEx_SMOC * 2.306;
geoguncertainty_NMOC = geogSEx_NMOC * 2.306;
geoguncertainty_NAMLD = geogSEx_NAMLD * 2.306;
geoguncertainty_SOMLD = geogSEx_SOMLD * 2.306;


%% CO2 simulations
desc = {'cha2nI','cha2eI','cha2fI','rup2nI','rup2fI','pri2nI','pri2fI'};

CO2mean_SMOC = zeros(length(desc),1); 
CO2mean_NMOC = zeros(length(desc),1); 
CO2mean_NAMLD = zeros(length(desc),1); 
CO2mean_SOMLD = zeros(length(desc),1); 

for i = 1:length(desc)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Find higher CO2
    descdif = [desc_str(1:3),'4',desc_str(5:6)];
    
    % Find change
    CO2mean_SMOC(i) = S_MOC2.(desc_str) - S_MOC2.(descdif);
    CO2mean_NMOC(i) = N_MOC2.(desc_str) - N_MOC2.(descdif);
    CO2mean_NAMLD(i) = NAtl_MLD2.(desc_str) - NAtl_MLD2.(descdif);
    CO2mean_SOMLD(i) = SO_MLD2.(desc_str) - SO_MLD2.(descdif);

end

% Calculate �95% uncertainty of the mean
CO2n = length(CO2mean_SOMLD); %7

% Calculate the standard error
CO2SEx_SMOC = std(CO2mean_SMOC)/sqrt(CO2n);
CO2SEx_NMOC = std(CO2mean_NMOC)/sqrt(CO2n);
CO2SEx_NAMLD = std(CO2mean_NAMLD)/sqrt(CO2n);
CO2SEx_SOMLD = std(CO2mean_SOMLD)/sqrt(CO2n);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
CO2uncertainty_SMOC = CO2SEx_SMOC * 2.447;
CO2uncertainty_NMOC = CO2SEx_NMOC * 2.447;
CO2uncertainty_NAMLD = CO2SEx_NAMLD * 2.447;
CO2uncertainty_SOMLD = CO2SEx_SOMLD * 2.447;
