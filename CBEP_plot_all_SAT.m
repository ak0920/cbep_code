% CBEP_plot_all_SAT
% 
% Plots all of the SAT response pairs for the boundary condition ensemble
% (Supp. Fig. 2 for the CBEP paper)
% 
% Lots of copy-pasting...
% 

% Load data
CBEP_loaddata

% Set some figure properties
load('RBcolgradcon.mat')
font = 'arial';
fontsz = 16;
figure
colormap(RBcolgradcon)
set(gcf, 'color', 'w');

% Start plotting
subplot('position', [0.1 0.80 0.165 0.165])
imagesc(SAT.cha4eI-SAT.cha4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: cha4e-cha4n')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.275 0.80 0.165 0.165])
imagesc(SAT.cha4fI-SAT.cha4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: cha4f-cha4n')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.45 0.80 0.165 0.165])
imagesc(SAT.cha2eI-SAT.cha2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: cha2e-cha2n')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.625 0.80 0.165 0.165])
imagesc(SAT.cha2fI-SAT.cha2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: cha2f-cha2n')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.8 0.80 0.165 0.165])
imagesc(SAT.rup4fI-SAT.rup4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: rup4f-rup4n')
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.1 0.605 0.165 0.165])
imagesc(SAT.rup2eI-SAT.rup2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: rup2e-rup2n')
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.275 0.605 0.165 0.165])
imagesc(SAT.rup2fI-SAT.rup2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: rup2f-rup2n')
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.45 0.605 0.165 0.165])
imagesc(SAT.pri4fI-SAT.pri4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: pri4f-pri4n')
contour(LSM.pri4nI,[0 0],'k')

subplot('position', [0.625 0.605 0.165 0.165])
imagesc(SAT.pri2fI-SAT.pri2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Ice: pri2f-pri2n')
contour(LSM.pri4nI,[0 0],'k')

subplot('position', [0.8 0.605 0.165 0.165])
imagesc(SAT.cha4nI-SAT.rup4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: cha4n-rup4n')
contour(LSM.rup4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.1 0.41 0.165 0.165])
imagesc(SAT.cha4fI-SAT.rup4fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: cha4f-rup4f')
contour(LSM.rup4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.275 0.41 0.165 0.165])
imagesc(SAT.cha2nI-SAT.rup2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: cha2n-rup2n')
contour(LSM.rup4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.45 0.41 0.165 0.165])
imagesc(SAT.cha2eI-SAT.rup2eI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: cha2e-rup2e')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.625 0.41 0.165 0.165])
imagesc(SAT.cha2fI-SAT.rup2fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: cha2f-rup2f')
contour(LSM.rup4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.8 0.41 0.165 0.165])
imagesc(SAT.rup4nI-SAT.pri4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: rup4n-pri4n')
contour(LSM.pri4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.1 0.215 0.165 0.165])
imagesc(SAT.rup4fI-SAT.pri4fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: rup4f-pri4f')
contour(LSM.pri4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.275 0.215 0.165 0.165])
imagesc(SAT.rup2nI-SAT.pri2nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: rup2n-pri2n')
contour(LSM.pri4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.45 0.215 0.165 0.165])
imagesc(SAT.rup2fI-SAT.pri2fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('Geog.: rup2f-pri2f')
contour(LSM.pri4nI,[0 0],'color',[0.5 0.5 0.5])
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.625 0.215 0.165 0.165])
imagesc(SAT.cha2nI-SAT.cha4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: cha2n-cha4n')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.8 0.215 0.165 0.165])
imagesc(SAT.cha2eI-SAT.cha4eI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: cha2e-cha4e')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.1 0.02 0.165 0.165])
imagesc(SAT.cha2fI-SAT.cha4fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: cha2f-cha4f')
contour(LSM.cha4nI,[0 0],'k')

subplot('position', [0.275 0.02 0.165 0.165])
imagesc(SAT.rup2nI-SAT.rup4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: rup2n-rup4n')
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.45 0.02 0.165 0.165])
imagesc(SAT.rup2fI-SAT.rup4fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: rup2f-rup4f')
contour(LSM.rup4nI,[0 0],'k')

subplot('position', [0.625 0.02 0.165 0.165])
imagesc(SAT.pri2nI-SAT.pri4nI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: pri2n-pri4n')
contour(LSM.pri4nI,[0 0],'k')

subplot('position', [0.8 0.02 0.165 0.165])
imagesc(SAT.pri2fI-SAT.pri4fI)
caxis([-10 10])
axis off; box on; hold on
set(gca,'fontname',font,'fontsize',fontsz)
title('CO2: pri2f-pri4f')
contour(LSM.pri4nI,[0 0],'k')


%% Add colorbar
subplot('Position', [1.7 0.19 0.25 0.33])
imagesc(SAT.pri2fI-SAT.pri4fI)
caxis([-10 10])

color = colorbar('location', 'EastOutside', 'Position', [0.07 0.25 0.02 0.5]);
ylabel(color, 'Annual mean SAT change (�C)', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [-10 10], 'YAxisLocation','left', 'fontname', font, 'fontsize', fontsz) % this range is probably bigger than is necessary

