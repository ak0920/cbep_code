% CBEP_load_mean_values
% 
% This script loads all of the data required for Figure 2 of the CBEP paper
% 

% Load raw data
CBEP_loaddata


% Generate area grids
proc_latlon_area
areas_frac = rot90(areas_frac);
areas_abs = rot90(areas_abs);


% Generate various averaging areas for Antarctic land/Southern Ocean
% For Chattian:
SOarea_cha = areas_abs(~isnan(SST.cha4fI(62:73,:)));
SOfrac_cha = SOarea_cha/sum(sum(SOarea_cha));
Antarea_cha = areas_abs(isnan(SST.cha4fI(62:72,:)));
Antfrac_cha = Antarea_cha/sum(sum(Antarea_cha));

% For Rupelian (Getech):
SOarea_rup = areas_abs(~isnan(SST.rup4fI(62:73,:)));
SOfrac_rup = SOarea_rup/sum(sum(SOarea_rup));
Antarea_rup = areas_abs(isnan(SST.rup4fI(62:72,:)));
Antfrac_rup = Antarea_rup/sum(sum(Antarea_rup));

% For Priabonian:
SOarea_pri = areas_abs(~isnan(SST.pri4fI(62:73,:)));
SOfrac_pri = SOarea_pri/sum(sum(SOarea_pri));
Antarea_pri = areas_abs(isnan(SST.pri4fI(62:72,:)));
Antfrac_pri = Antarea_pri/sum(sum(Antarea_pri));

% For Rupelian (Robertsons) with open DP:
SOarea_spo = areas_abs(~isnan(SST.end2fO(62:73,:)));
SOfrac_spo = SOarea_spo/sum(sum(SOarea_spo));
Antarea_spo = areas_abs(isnan(SST.end2fO(62:72,:)));
Antfrac_spo = Antarea_spo/sum(sum(Antarea_spo));

% For Rupelian (Robertsons) with closed DP:
SOarea_spc = areas_abs(~isnan(SST.end2fC(62:73,:)));
SOfrac_spc = SOarea_spc/sum(sum(SOarea_spc));
Antarea_spc = areas_abs(isnan(SST.end2fC(62:72,:)));
Antfrac_spc = Antarea_spc/sum(sum(Antarea_spc));


% List of simulation descriptions
desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
    'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
    'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};


% Create blank arrays to add mean values into
DP_ACC = nan(length(desc),1);
SO_SST = nan(length(desc),1);
AntP = nan(length(desc),1);
AntSAT = nan(length(desc),1);
SATgam = nan(length(desc),1);


%% Calculate DP flow and Gamburtsev summer temperature:
% These variables are calculated the same way for all paleogeogs

for i = 1:length(desc)
    
%   Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Note: (70,38) is over Antarctic land and (46,38) is over S. American 
    % land for all stages, difference them to find passage through flow:
    DP_ACC(i) = STR.(desc_str)(70,38) - STR.(desc_str)(46,38); % save as array for easy analysis
    DP_ACC2.(desc_str) = STR.(desc_str)(70,38) - STR.(desc_str)(46,38); % also save as structure incase individual simulation data is required
    
    % Note: Gamburtsev area index = (68:69,67:72)
    SATgam(i) = sum(sum(SATdjf.(desc_str)(68:69,67:72).*areas_abs(68:69,67:72))/sum(sum(areas_abs(68:69,67:72))));
    SATgam2.(desc_str) = sum(sum(SATdjf.(desc_str)(68:69,67:72).*areas_abs(68:69,67:72))/sum(sum(areas_abs(68:69,67:72))));

end


%% Calculate Southern Ocean SST and mean Antarctic P
% These variables depend on the size of the SO/Antarctica, which varies for
% each palaeogeog., hence why there are a number of if statements to use
% the correct area weighting of land/ocean for the palaeogeographies.

for i = 1:length(desc)
    
%   Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % For Chattian
    if desc_str(1) == 'c'
        SSTtemp=SST.(desc_str)(62:73,:); % subset the Southern Ocean > 61.25 �S
        SO_SST(i) = sum(SSTtemp(LSM.cha4fI(62:73,:)).*SOfrac_cha); % calculate the mean
        SO_SST2.(desc_str) = sum(SSTtemp(LSM.cha4fI(62:73,:)).*SOfrac_cha);
        
        Ptemp=P.(desc_str)(62:72,:); % subset the atmosphere > 61.25 �S
        AntP(i) = sum(Ptemp(isnan(SST.cha4fI(62:72,:))).*Antfrac_cha); % calculate the mean over land areas
        AntP2.(desc_str) = sum(Ptemp(isnan(SST.cha4fI(62:72,:))).*Antfrac_cha);

        SATtemp=SAT.(desc_str)(62:72,:); % subset the atmosphere > 61.25 �S
        AntSAT(i) = sum(SATtemp(isnan(SST.cha4fI(62:72,:))).*Antfrac_cha); % calculate the mean over land areas
        AntSAT2.(desc_str) = sum(SATtemp(isnan(SST.cha4fI(62:72,:))).*Antfrac_cha);
   
    % For Rupelian
    else if desc_str(1) == 'r'
            SSTtemp=SST.(desc_str)(62:73,:);
            SO_SST(i) = sum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
            SO_SST2.(desc_str) = sum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
            
            Ptemp=P.(desc_str)(62:72,:);
            AntP(i) = sum(Ptemp(isnan(SST.rup4fI(62:72,:))).*Antfrac_rup);
            AntP2.(desc_str) = sum(Ptemp(isnan(SST.rup4fI(62:72,:))).*Antfrac_rup);

            SATtemp=SAT.(desc_str)(62:72,:);
            AntSAT(i) = sum(SATtemp(isnan(SST.rup4fI(62:72,:))).*Antfrac_rup);
            AntSAT2.(desc_str) = sum(SATtemp(isnan(SST.rup4fI(62:72,:))).*Antfrac_rup);
        
        % For Priabonian
        else if desc_str(1) == 'p'
                SSTtemp=SST.(desc_str)(62:73,:);
                SO_SST(i) = sum(SSTtemp(LSM.pri4fI(62:73,:)).*SOfrac_pri);
                SO_SST2.(desc_str) = sum(SSTtemp(LSM.pri4fI(62:73,:)).*SOfrac_pri);
                
                Ptemp=P.(desc_str)(62:72,:);
                AntP(i) = sum(Ptemp(isnan(SST.pri4fI(62:72,:))).*Antfrac_pri);
                AntP2.(desc_str) = sum(Ptemp(isnan(SST.pri4fI(62:72,:))).*Antfrac_pri);

                SATtemp=SAT.(desc_str)(62:72,:);
                AntSAT(i) = sum(SATtemp(isnan(SST.pri4fI(62:72,:))).*Antfrac_pri);
                AntSAT2.(desc_str) = sum(SATtemp(isnan(SST.pri4fI(62:72,:))).*Antfrac_pri);
    
            % For Robertsons (open DP)
            else if desc_str(6) == 'O'
                    SSTtemp=SST.(desc_str)(62:73,:);
%                     SO_SST(i) = sum(SSTtemp(~isnan(SST.end2fO(62:73,:))).*SOfrac_spo);
%                     SO_SST2.(desc_str) = sum(SSTtemp(~isnan(SST.end2fO(62:73,:))).*SOfrac_spo);
                    SO_SST(i) = nansum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
                    SO_SST2.(desc_str) = nansum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
                    
                    Ptemp=P.(desc_str)(62:72,:);
                    AntP(i) = sum(Ptemp(isnan(SST.end2fO(62:72,:))).*Antfrac_spo);
                    AntP2.(desc_str) = sum(Ptemp(isnan(SST.end2fO(62:72,:))).*Antfrac_spo);

                    SATtemp=SAT.(desc_str)(62:72,:);
                    AntSAT(i) = sum(SATtemp(isnan(SST.end2fO(62:72,:))).*Antfrac_spo);
                    AntSAT2.(desc_str) = sum(SATtemp(isnan(SST.end2fO(62:72,:))).*Antfrac_spo);
    
                % For Robertsons (closed DP)
                else
                    SSTtemp=SST.(desc_str)(62:73,:);
%                     SO_SST(i) = sum(SSTtemp(~isnan(SST.end2fC(62:73,:))).*SOfrac_spc);
%                     SO_SST2.(desc_str) = sum(SSTtemp(~isnan(SST.end2fC(62:73,:))).*SOfrac_spc);
                    SO_SST(i) = nansum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
                    SO_SST2.(desc_str) = nansum(SSTtemp(LSM.rup4fI(62:73,:)).*SOfrac_rup);
                    
                    Ptemp=P.(desc_str)(62:72,:);
                    AntP(i) = sum(Ptemp(isnan(SST.end2fC(62:72,:))).*Antfrac_spc);
                    AntP2.(desc_str) = sum(Ptemp(isnan(SST.end2fC(62:72,:))).*Antfrac_spc);
  
                    SATtemp=SAT.(desc_str)(62:72,:);
                    AntSAT(i) = sum(SATtemp(isnan(SST.end2fC(62:72,:))).*Antfrac_spc);
                    AntSAT2.(desc_str) = sum(SATtemp(isnan(SST.end2fC(62:72,:))).*Antfrac_spc);
    
                end
            end
        end
    end
end

