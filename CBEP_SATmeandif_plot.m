% CBEP_SATmeandif_plot
% 
% This script makes a plot of the mean spatial change due to AIS growth,
% palaeogeographic change and CO2 drop (Figure 1, left hand column)
% 

% Load the mean spatial change data
CBEP_SATuncertainty % This also loads CBEP_SATmeandif

% Load the lat-lon grids for using the mapping toolbox
if strcmp(pwd,'/Users/ak0920/Data/EO_more')
    run('../EO_code/load_latlon.m')
else load_latlon
end


%% Setup some figure properties
close(gcf)
figure

load('RBcolgradcon.mat')
colormap(RBcolgradcon)
caxis([-40 40])
font = 'arial';
fontsz = 16;
set(gcf, 'color', 'w');
latlim = [-90 90]; 
lonlim = [0 356.25];


%% Plot ice dif 
% Set plot position
subplot('position', [0.15 0.6875 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,...
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(SATmeanicedif),[-10:10], 'LineStyle', 'none')

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
colormap(RBcolgradcon)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
caxis([-10 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['AIS growth: n=',num2str(length(SATannicedif(1,1,:)))])

hold on


% Add hatching where difference is less than uncertainty
hatarea = flipud(abs(SATmeanicedif)<(iceuncertainty));

% Add the hatching
% (Added to every other gridcell and scaled to latitude, getting smaller 
% nearer the poles to reduce clutter)
for i = 1:2:length(hatarea(:,1));
    for j = 1:2:length(hatarea(1,:));
        if hatarea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'x','color',[0.3 0.3 0.3],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add stippling where all n model pairs agree on direction of change
stiparea = flipud(sum(SATannicedif>0,3) == length(SATannicedif(1,1,:)) | sum(SATannicedif>0,3) == 0);

% Add the stippling
% (Added to every other gridcell and scaled to latitude, getting smaller 
% nearer the poles to reduce clutter)
for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add LSMs, with oldest in palest grey, most recent in black
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Plot geog dif 
% Set plot position
subplot('position', [0.15 0.3675 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(SATmeangeogdif),[-10:10], 'LineStyle', 'none') 

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
colormap(RBcolgradcon);
caxis([-10 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['Palaeogeographic change: n=',num2str(length(SATanngeogdif(1,1,:)))])

hold on

% Add hatching
hatarea = flipud(abs(SATmeangeogdif)<(geoguncertainty));

for i = 1:2:length(hatarea(:,1));
    for j = 1:2:length(hatarea(1,:));
        if hatarea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'x','color',[0.4 0.4 0.4],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add stippling
stiparea = flipud(sum(SATanngeogdif>0,3) == length(SATanngeogdif(1,1,:)) | sum(SATanngeogdif>0,3) == 0);

for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add LSMs
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.5 0.5 0.5],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.25 0.25 0.25],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Plot CO2 dif 
% Set plot position
subplot('position', [0.15 0.0475 0.8 0.275])

% Plot the main map data
axesm('MapProjection','Robinson', 'MapLatLim', latlim,... 
    'MapLonLim', lonlim, 'MLineLocation', 60,...
    'PlineLocation', 60, 'MLabelParallel', 'south')
contourfm(lat,lon,flipud(SATmeanCO2dif),[-10:10], 'LineStyle', 'none')

% Tidy up
framem('FEdgeColor', 'black', 'FLineWidth', 1)
gridm('Gcolor',[0.3 0.3 0.3])
tightmap
box off
axis off
colormap(RBcolgradcon);
caxis([-10 10])
set(gca,'fontname',font,'fontsize',fontsz)
title(['CO_2 drop: n=',num2str(length(SATannCO2dif(1,1,:)))])

% Add the colour bar
color = colorbar('location', 'EastOutside', 'Position', [0.1 0.25 0.03 0.5]);
ylabel(color, 'Annual mean SAT change (�C)', 'fontname', font, 'fontsize', fontsz)
set(color,  'ylim', [-10 10], 'YAxisLocation','left', 'fontname', font, 'fontsize', fontsz) 

hold on

% Add hatching
hatarea = flipud(abs(SATmeanCO2dif)<(CO2uncertainty));

for i = 1:2:length(hatarea(:,1));
    for j = 1:2:length(hatarea(1,:));
        if hatarea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'x','color',[0.5 0.5 0.5],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add stippling
stiparea = flipud(sum(SATannCO2dif>0,3) == length(SATannCO2dif(1,1,:)) | sum(SATannCO2dif>0,3) == 0);

for i = 1:2:length(stiparea(:,1));
    for j = 1:2:length(stiparea(1,:));
        if stiparea(i,j)==1
            plotm(lat(i),lon(j)+1.875,'.','color',[0.1 0.1 0.1],'markersize',8-abs(lat(i))/30)
        end
    end
end

% Add LSMs
contourm(lat,lon,double(flipud(LSM.pri4nI)),[0 0],'linecolor',[0.4 0.4 0.4],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.rup4nI)),[0 0],'linecolor',[0.2 0.2 0.2],'linewidth',1.5)
contourm(lat,lon,double(flipud(LSM.cha4nI)),[0 0],'k','linewidth',1.5)


%% Final tidy up
% Resize the figure
x0=20;
y0=10;
width=600;
height=1200;
set(gcf,'units','points','position',[x0,y0,width,height])
