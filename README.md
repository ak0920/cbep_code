# README #

This repo includes all of the analysis code used for the Kennedy-Asser et al. 2019 Paleoceanography and Paleoclimatology paper.

The Matlab scripts have been written to run on my local machine and have not been made/tested for universal usage, so will likely require some coaxing and tweaking to get them to work elsewhere.

### Contents ###

* .m files starting "CBEP_" are those which do the analysis and plotting for the paper
* Other .m files have been included where they are used by the CBEP_... scripts, but which were not written specifically for this work (or by me in some cases)
* .mat files generally for plotting purposes, e.g. colour scales

### Other notes ###

* These scripts will require the Matlab mapping tool box
* Currently (22/5/2019) there may also be some additional scripts which are not necessary for the core work presented in the paper - I am yet to strip this repo down to the bare essentials
* Any questions, please get in touch
