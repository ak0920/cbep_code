% CBEP_check_lapse
% 
% Find out the approximate contribution of lapse rate vs albedo for cooling
% over Antarctica due to glaciation, as requestred by one of the reviewers.
% This figure is not included in the paper.
% 

%% Load data

% Load topo data
rupntop    = rot90(ncread('../Getech/ancil/rup/rup_010.nc', 'oroguk'));
chantop    = rot90(ncread('../Getech/ancil/cha/cha_010.nc', 'oroguk'));
printop    = rot90(ncread('../Getech/ancil/pri/pri_010.nc', 'oroguk'));
rupftop    = rot90(ncread('../Getech/ancil/rup/rup_025.nc', 'oroguk'));
chaetop    = rot90(ncread('../Getech/ancil/cha/cha_025.nc', 'oroguk'));
chaftop    = rot90(ncread('../Getech/ancil/cha/cha_040.nc', 'oroguk'));
primtop    = rot90(ncread('../Getech/ancil/pri/pri_025.nc', 'oroguk'));
priftop    = rot90(ncread('../Getech/ancil/pri/pri_045.nc', 'oroguk'));

% Load temp and height data for an ice free and a glaciated run
if ~exist('TempAtm','var')
    TempAtm = ncread('../EO_data/tdluya.pcclann.nc','temp_mm_p');
    HeightAtm = ncread('../EO_data/tdluya.pcclann.nc','ht_mm_p');
    
    % Take a point over the centre of the ice sheet
    TempAtm = squeeze(TempAtm(20,70,:));
    HeightAtm = squeeze(HeightAtm(20,70,:))+chantop(4,20);
    
    % Interpolate the heights onto a regular spacing
    HeightAtmi = [1e4 1.1e4 1.2e4 1.3e4 1.4e4 1.5e4];
    TempAtmi = interp1(HeightAtm, TempAtm, HeightAtmi, 'linear');
    
    % Store these variables
    TempAtm2.cha4nI = TempAtmi;
    HeightAtm2.cha4nI = HeightAtmi;
    
    
    TempAtm = ncread('../EO_data/tdzsea.pcclann.nc','temp_mm_p');
    HeightAtm = ncread('../EO_data/tdzsea.pcclann.nc','ht_mm_p');
    
    % Take a point over the centre of the ice sheet
    TempAtm = squeeze(TempAtm(20,70,:));
    HeightAtm = squeeze(HeightAtm(20,70,:))+chaftop(4,20);
    
    % Interpolate the heights onto a regular spacing
    HeightAtmi = [1e4 1.1e4 1.2e4 1.3e4 1.4e4 1.5e4];
    TempAtmi = interp1(HeightAtm, TempAtm, HeightAtmi, 'linear');
    
    % Store these variables
    TempAtm2.cha4fI = TempAtmi;
    HeightAtm2.cha4fI = HeightAtmi;
    
end

%% Plot the temperature profile with height in the atmosphere for an ice free and a glaciated run
figure; hold on
plot(TempAtm2.cha4fI,HeightAtm2.cha4fI,'-b','linewidth',3)
plot(TempAtm2.cha4nI,HeightAtm2.cha4nI,'-r','linewidth',3)
TempAtm2.cha4fI-TempAtm2.cha4nI

