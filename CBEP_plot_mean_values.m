% CBEP_plot_mean_values
% 
% This script generates Figure 2 for the CBEP paper
% 
% Each marker shape, colour, spacing and fill is defined by the simulation
% it comes from. There would be a clever coding way to do this I'm sure,
% but instead I have manually assigned them... Hence the length
% 

% Load data if necessary
if ~exist('DP_ACC')
    CBEP_load_mean_values
end
if ~exist('geogmean_DP')
    CBEP_load_mean_changes
end

% Set some defaults
fontnm = 'arial';
fontsz = 14;
% Colours
red = [0.850000000000000,0.0872549042105675,0];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
grey = [0.6 0.6 0.6];
lgrey = [0.8 0.8 0.8];


%% Plotting

figure

%% DP flow absolute:
subplot('position', [0.125 0.81 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[0 0 150 150],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[0 0 150 150],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};
% (See CBEP_load_mean_values.m)

% Chattian 4x runs:
plot(3,DP_ACC(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,DP_ACC(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,DP_ACC(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,DP_ACC(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2nI
plot(3.4,DP_ACC(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2eI
plot(3.5,DP_ACC(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2fI

% Rupelian 4x runs:
plot(2,DP_ACC(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4nI
plot(2.1,DP_ACC(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4fI

% Rupelian 2x runs:
plot(2.2,DP_ACC(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2nI
plot(2.3,DP_ACC(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2eI
plot(2.4,DP_ACC(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2fI

% Priabonian 4x runs:
plot(1.1,DP_ACC(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4nI
plot(1.2,DP_ACC(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4fI

% Priabonian 2x runs:
plot(1.3,DP_ACC(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2nI
plot(1.4,DP_ACC(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2fI


% Paul's runs (all must have an open DP obviously):
plot(4.4,DP_ACC(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.3,DP_ACC(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.2,DP_ACC(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.1,DP_ACC(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO

% Tidy the figure
set(gca,'XTick',[],'Xlim',[0.9 5], 'fontname',fontnm, 'fontsize',fontsz)
ylabel('Drake Passage flow (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1, 135,'a', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% DP flow change:
subplot('position', [0.675 0.81 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Plot symbols once in order for legend
plot(1,mean(icemean_DP),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% ice growth
plot(2,mean(geogmean_DP),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% paleogeog
plot(3,mean(CO2mean_DP),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% CO2

% Plot uncertainty bars with symbols on top
% Ice growth effect:
plot([1 1],[mean(icemean_DP)-iceuncertainty_DP mean(icemean_DP)+iceuncertainty_DP],'-','color',grey,'linewidth',5); % uncert (for legend)
plot([1 1],[min(icemean_DP) max(icemean_DP)],'-','color',lgrey,'linewidth',2); % range
plot([1 1],[mean(icemean_DP)-iceuncertainty_DP mean(icemean_DP)+iceuncertainty_DP],'-','color',grey,'linewidth',5); % uncert
plot(1,mean(icemean_DP),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean
% Paleogeog effect:
plot([2 2],[min(geogmean_DP) max(geogmean_DP)],'-','color',lgrey,'linewidth',2); % range
plot([2 2],[mean(geogmean_DP)-geoguncertainty_DP mean(geogmean_DP)+geoguncertainty_DP],'-','color',grey,'linewidth',5); % uncert
plot(2,mean(geogmean_DP),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean
% CO2 effect:
plot([3 3],[min(CO2mean_DP) max(CO2mean_DP)],'-','color',lgrey,'linewidth',2); % range
plot([3 3],[mean(CO2mean_DP)-CO2uncertainty_DP mean(CO2mean_DP)+CO2uncertainty_DP],'-','color',grey,'linewidth',5); % uncert
plot(3,mean(CO2mean_DP),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean

% Tidy up
ylabel('Drake Passage flow change (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
legend('Ice growth','Palaeogeog.','pCO_2','U_t','Range','Location',[0.675 0.04 0.2 0.1])
text(0.7, 36,'b', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Southern Ocean SST absolute:
subplot('position', [0.125 0.60 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[2 2 12 12],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[2 2 12 12],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x runs:
plot(3,SO_SST(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,SO_SST(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,SO_SST(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,SO_SST(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2nI
plot(3.4,SO_SST(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2eI
plot(3.5,SO_SST(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2fI

% Rupelian 4x runs:
plot(2,SO_SST(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4nI
plot(2.1,SO_SST(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4fI

% Rupelian 2x runs:
plot(2.2,SO_SST(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2nI
plot(2.3,SO_SST(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2eI
plot(2.4,SO_SST(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2fI

% Priabonian 4x runs:
plot(1.1,SO_SST(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4nI
plot(1.2,SO_SST(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4fI

% Priabonian 2x runs:
plot(1.3,SO_SST(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2nI
plot(1.4,SO_SST(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2fI

% Paul's runs:
plot(4.4,SO_SST(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,SO_SST(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,SO_SST(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,SO_SST(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,SO_SST(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,SO_SST(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,SO_SST(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,SO_SST(23),'^','markersize',10,'Color',red)% end3nC

% Tidy up
set(gca,'XTick',[],'Xlim',[0.9 5],'Ylim',[2 12], 'fontname',fontnm, 'fontsize',fontsz)
ylabel('Annual mean Southern Ocean SST (�C)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1, 11,'c', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Southern Ocean SST change:
subplot('position', [0.675 0.60 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero line
plot([0 4],[0 0],'-k')

% Plot uncertainty bars and mean values:
plot([1 1],[min(icemean_SOSST) max(icemean_SOSST)],'-','color',lgrey,'linewidth',2);% ice growth range
plot([1 1],[mean(icemean_SOSST)-iceuncertainty_SOSST mean(icemean_SOSST)+iceuncertainty_SOSST],'-','color',grey,'linewidth',5);% ice growth uncert
plot(1,mean(icemean_SOSST),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% ice growth mean
plot([2 2],[min(geogmean_SOSST) mean(geogmean_SOSST)],'-','color',lgrey,'linewidth',2);% paleogeog range
plot([2 2],[mean(geogmean_SOSST)-geoguncertainty_SOSST mean(geogmean_SOSST)+geoguncertainty_SOSST],'-','color',grey,'linewidth',5);% paleogeog uncert
plot(2,mean(geogmean_SOSST),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% paleogeog mean
plot([3 3],[min(CO2mean_SOSST) max(CO2mean_SOSST)],'-','color',lgrey,'linewidth',2);% CO2 range
plot([3 3],[mean(CO2mean_SOSST)-CO2uncertainty_SOSST mean(CO2mean_SOSST)+CO2uncertainty_SOSST],'-','color',grey,'linewidth',5);% CO2 uncert
plot(3,mean(CO2mean_SOSST),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% CO2 mean

% Tidy up
ylabel('Annual mean Southern Ocean SST change (�C)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7, 1.4,'d', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Antarctic P absolute:
subplot('position', [0.125 0.39 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[200 200 800 800],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[200 200 800 800],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x runs:
plot(3,AntP(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,AntP(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,AntP(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,AntP(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2nI
plot(3.4,AntP(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2eI
plot(3.5,AntP(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2fI

% Rupelian 4x runs:
plot(2,AntP(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup4nI
plot(2.1,AntP(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup4fI

% Rupelian 2x runs:
plot(2.2,AntP(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2nI
plot(2.3,AntP(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2eI
plot(2.4,AntP(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2fI

% Priabonian 4x runs:
plot(1.1,AntP(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri4nI
plot(1.2,AntP(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri4fI

% Priabonian 2x runs:
plot(1.3,AntP(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri2nI
plot(1.4,AntP(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri2fI

% Paul's runs: 
plot(4.4,AntP(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,AntP(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,AntP(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,AntP(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,AntP(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,AntP(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,AntP(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,AntP(23),'^','markersize',10,'Color',red)% end3nC

% Tidy up
set(gca,'XTick',[],'Xlim',[0.9 5], 'YLim',[200 800], 'fontname',fontnm, 'fontsize',fontsz)
ylabel('Antarctic P (mm a^-^1)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1, 740,'e', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Antarctic P change:
subplot('position', [0.675 0.39 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero lone
plot([0 4],[0 0],'-k')

% Uncertainty bar and mean values
plot([1 1],[min(icemean_AntP) max(icemean_AntP)],'-','color',lgrey,'linewidth',2); % ice growth range
plot([1 1],[mean(icemean_AntP)-iceuncertainty_AntP mean(icemean_AntP)+iceuncertainty_AntP],'-','color',grey,'linewidth',5); % ice growth uncert
plot(1,mean(icemean_AntP),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % ice growth mean
plot([2 2],[min(geogmean_AntP) max(geogmean_AntP)],'-','color',lgrey,'linewidth',2);% paleogeog range
plot([2 2],[mean(geogmean_AntP)-geoguncertainty_AntP mean(geogmean_AntP)+geoguncertainty_AntP],'-','color',grey,'linewidth',5); % paleogeog uncert
plot(2,mean(geogmean_AntP),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % paleogeog mean
plot([3 3],[min(CO2mean_AntP) max(CO2mean_AntP)],'-','color',lgrey,'linewidth',2); % CO2 range
plot([3 3],[mean(CO2mean_AntP)-CO2uncertainty_AntP mean(CO2mean_AntP)+CO2uncertainty_AntP],'-','color',grey,'linewidth',5); % CO2 uncert
plot(3,mean(CO2mean_AntP),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % CO2 mean

% Tidy up
ylabel('Antarctic P change (mm a^-^1)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7, 140,'f', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Gamburtsev SAT absolute: (only ice free are plotted to make scale better)
subplot('position', [0.125 0.18 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[5 5 20 20],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[5 5 20 20],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x run:
plot(3,SATgam(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha4nI

% Chattian 2x run:
plot(3.3,SATgam(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2nI

% Rupelian 4x run
plot(2,SATgam(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup4nI

% Rupelian 2x run:
plot(2.2,SATgam(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2nI

% Priabonian 4x run:
plot(1.1,SATgam(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri4nI

% Priabonian 2x run:
plot(1.3,SATgam(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri2nI

% Paul's runs:
plot(4.3,SATgam(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) % end2nO
plot(4.7,SATgam(19),'^','markersize',10,'Color',blue) % end2nC
plot(4.1,SATgam(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red) % end3nO
plot(4.5,SATgam(23),'^','markersize',10,'Color',red) % end3nC

% Set bottom x-axis labels
names = {'Priabonian'; 'Rupelian'; 'Chattian'; 'Rupelian (alt.)'};
set(gca,'xtick',[1.3, 2.2, 3.225, 4.4],'xticklabel',names,'Xlim',[0.9 5], 'fontname',fontnm, 'fontsize',fontsz)

% Tidy up
ylabel('Gamburtsev summer SAT (�C)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1,18.5,'g', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Gamburtsev SAT change: (ice growth not shown to make scale better)
subplot('position', [0.675 0.18 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero line
plot([0 4],[0 0],'-k')

% Add mean values and uncertainty
plot([2 2],[min(geogmean_SATgam) max(geogmean_SATgam)],'-','color',lgrey,'linewidth',2); % paleogeog range
plot([2 2],[mean(geogmean_SATgam)-geoguncertainty_SATgam mean(geogmean_SATgam)+geoguncertainty_SATgam],'-','color',grey,'linewidth',5); % paleogeog uncert
plot(2,mean(geogmean_SATgam),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k'); % paleogeog mean
plot([3 3],[min(CO2mean_SATgam) max(CO2mean_SATgam)],'-','color',lgrey,'linewidth',2); % paleogeog range
plot([3 3],[mean(CO2mean_SATgam)-CO2uncertainty_SATgam mean(CO2mean_SATgam)+CO2uncertainty_SATgam],'-','color',grey,'linewidth',5); % paleogeog mean
plot(3,mean(CO2mean_SATgam),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k'); % paleogeog mean

% Tidy up
ylabel('Gamburtsev summer SAT change (�C)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7, 8,'h', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Manually creat legend for left hand column
leg1ax = axes('Position', [0.125 0.0175 0.525 0.1]);
hold on
plot(2.2,2.8,'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.6,2.8,'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.2,1.8,'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red) 
plot(2.6,1.8,'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.2,0.8,'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red) 
plot(2.6,0.8,'^','markersize',10,'Color',red)
text(1, 3,'Shape:', 'fontname',fontnm, 'fontsize',fontsz)
text(1, 2,'Colour:', 'fontname',fontnm, 'fontsize',fontsz)
text(1, 1,'Fill:', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 3,'No ice / Glaciated', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 2,'High / Low pCO_2', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 1,'Open / Closed Drake Passage', 'fontname',fontnm, 'fontsize',fontsz)
set(gca,'xlim',[0.75 7],'ylim',[0 4],'xtick',[],'ytick',[])
% axis off
box on

% Final tidy up
set(gcf, 'color', 'w');
x0=10;
y0=10;
width=800;
height=1200;
set(gcf,'units','points','position',[x0,y0,width,height])
