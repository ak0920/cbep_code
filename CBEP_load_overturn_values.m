% CBEP_load_overturn_values
% 
% This script loads all of the data required for Figure 3 of the CBEP paper
% 
% Note: Although the MLD and northern overturning functions could also
% identify Pacific sinking, visual inspection of spatial MLD plots suggest
% the only northern hemisphere sinking is in the N. Atlantic for all
% simulations
% 

% Load raw data
CBEP_loaddata


% List of simulation descriptions
desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
    'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
    'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};


% Create blank arrays to add mean values into
SO_MLD = nan(length(desc),1);
NAtl_MLD = nan(length(desc),1);
S_MOC = nan(length(desc),1);
N_MOC = nan(length(desc),1);


%% Calculate values:
% Note: none of these are paleogeography specific

for i = 1:length(desc)
    
%   Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
     
    currentMLD = MLD.(desc_str);

    % Find maximum SH MLD
    SO_MLD(i) = max(max(currentMLD(37:73,:)));
    SO_MLD2.(desc_str) = max(max(currentMLD(37:73,:)));
    
    % Find maximum NH MLD (this is in N. Atlantic for all simulations)
    NAtl_MLD(i) = max(max(currentMLD(1:37,:)));
    NAtl_MLD2.(desc_str) = max(max(currentMLD(1:37,:)));
    
    % Remove large negatives where there is land
    mSTR_current = mSTR.(desc_str);
    mSTR_current(mSTR_current<-999) = nan;
    
    % Find the maximum southern overturning > 374 m from 90 �S to 32 �N
    S_MOC(i) = abs(nanmin(nanmin(mSTR_current(11:21,1:50))));
    S_MOC2.(desc_str) = abs(nanmin(nanmin(mSTR_current(11:21,1:50))));
    
    % Find the maximum northern overturning > 374 m from 17 �S to 80 �N
    N_MOC(i) = nanmax(nanmax(mSTR_current(11:21,30:70)));
    N_MOC2.(desc_str) = nanmax(nanmax(mSTR_current(11:21,30:70)));
end

