% CBEP_load_mean_changes
% 
% This calculates the changes in various climatic variables with changing 
% ice sheet, palaeogeog and CO2 from the Getech simulations (not Paul's)
% for use in Figure 2
% 

%% Load data and any prep work
CBEP_load_mean_values

% Load gridcell size averages
proc_latlon_area
areas_frac = rot90(areas_frac);
areas_abs = rot90(areas_abs);


%% AIS growth
desc = {'cha4eI','cha4fI','rup4fI','cha2fI','cha2eI','rup2fI','rup2eI','pri4fI','pri2fI'};

% Make empty arrays for global means:
icemean_SATglob = zeros(length(desc),1); % global mean SAT
icemean_SATgam = zeros(length(desc),1); % Gamburtsev summer mean SAT
icemean_SOSST = zeros(length(desc),1); % Southern Ocean SST
icemean_DP = zeros(length(desc),1); % DP flow
icemean_AntP = zeros(length(desc),1); % Antarctic mean P

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (glaciated identifiers) into useable string
    desc_cell = desc(i);
    desc_ice = sprintf('%s',desc_cell{:});
    
    % Find unglaciated identifiers
    desc_noice = [desc_ice(1:4),'n',desc_ice(6)];
    
    
    % Calculate global mean SAT
    % Save as structure (so mean value can be linked to exact simulation pair)
    SATicemean_glob_all.(desc_ice) = sum(sum(SAT.(desc_ice).*areas_frac)) - ...
        sum(sum(SAT.(desc_noice).*areas_frac));
    % Save as array (to make analysis easier)
    icemean_SATglob(i) = sum(sum(SAT.(desc_ice).*areas_frac)) - ...
        sum(sum(SAT.(desc_noice).*areas_frac));
    
    icemean_SATgam(i) = SATgam2.(desc_ice) - SATgam2.(desc_noice);
    icemean_SOSST(i) = SO_SST2.(desc_ice) - SO_SST2.(desc_noice);
    icemean_DP(i) = DP_ACC2.(desc_ice) - DP_ACC2.(desc_noice);
    icemean_AntP(i) = AntP2.(desc_ice) - AntP2.(desc_noice);

end

% Calculate �95% uncertainty on the mean
% Find n (no. of pairs)
icen = length(icemean_SATglob); %9

% Calculate the standard error
iceSEx_SATglob = std(icemean_SATglob)/sqrt(icen);
iceSEx_SATgam = std(icemean_SATgam)/sqrt(icen);
iceSEx_SOSST = std(icemean_SOSST)/sqrt(icen);
iceSEx_DP = std(icemean_DP)/sqrt(icen);
iceSEx_AntP = std(icemean_AntP)/sqrt(icen);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
iceuncertainty_SATglob = iceSEx_SATglob * 2.306;
iceuncertainty_SATgam = iceSEx_SATgam * 2.306;
iceuncertainty_SOSST = iceSEx_SOSST * 2.306;
iceuncertainty_DP = iceSEx_DP * 2.306;
iceuncertainty_AntP = iceSEx_AntP * 2.306;

%% Geog simulations
desc = {'cha4nI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI','rup2nI','rup2fI'};

geogmean_SATglob = zeros(length(desc),1);
geogmean_SATgam = zeros(length(desc),1);
geogmean_SOSST = zeros(length(desc),1); % Antarctic mean SAT
geogmean_DP = zeros(length(desc),1); % Antarctic mean SAT
geogmean_AntP = zeros(length(desc),1); % Antarctic mean SAT

for i = 1:length(desc)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Find previous equivalent palaeogeog
    if desc_str(1) == 'c'
        descdif = ['rup',desc_str(4:6)];
    else if desc_str(1) == 'r'
            descdif = ['pri',desc_str(4:6)];
        end
    end
    
    
    % Global mean SAT
    % Save as structure (so mean value can be linked to exact simulation pair)
    SATgeogmean_globI_all.(desc_str) = sum(sum(SAT.(desc_str).*areas_frac)) - ...
        sum(sum(SAT.(descdif).*areas_frac));
    % Save as array (to calculate mean easier)
    geogmean_SATglob(i) = sum(sum(SAT.(desc_str).*areas_frac)) - ...
        sum(sum(SAT.(descdif).*areas_frac));
    
    geogmean_SATgam(i) = SATgam2.(desc_str) - SATgam2.(descdif);
    geogmean_SOSST(i) = SO_SST2.(desc_str) - SO_SST2.(descdif);
    geogmean_DP(i) = DP_ACC2.(desc_str) - DP_ACC2.(descdif);
    geogmean_AntP(i) = AntP2.(desc_str) - AntP2.(descdif);

end

% Calculate �95% uncertainty of the mean
geogn = length(geogmean_SATglob); %9

% Calculate the standard error
geogSEx_SATglob = std(geogmean_SATglob)/sqrt(geogn);
geogSEx_SATgam = std(geogmean_SATgam)/sqrt(icen);
geogSEx_SOSST = std(geogmean_SOSST)/sqrt(icen);
geogSEx_DP = std(geogmean_DP)/sqrt(icen);
geogSEx_AntP = std(geogmean_AntP)/sqrt(icen);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
geoguncertainty_SATglob = geogSEx_SATglob * 2.306;
geoguncertainty_SATgam = geogSEx_SATgam * 2.306;
geoguncertainty_SOSST = geogSEx_SOSST * 2.306;
geoguncertainty_DP = geogSEx_DP * 2.306;
geoguncertainty_AntP = geogSEx_AntP * 2.306;


%% CO2 simulations
desc = {'cha2nI','cha2eI','cha2fI','rup2nI','rup2fI','pri2nI','pri2fI'};

CO2mean_SATglob = zeros(length(desc),1);
CO2mean_SATgam = zeros(length(desc),1);
CO2mean_SOSST = zeros(length(desc),1); 
CO2mean_DP = zeros(length(desc),1); 
CO2mean_AntP = zeros(length(desc),1); 

for i = 1:length(desc)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    
    % Find higher CO2
    descdif = [desc_str(1:3),'4',desc_str(5:6)];
    
    % Global mean SAT
    % Save as structure (so mean value can be linked to exact simulation pair)
    SATCO2mean_globI_all.(desc_str) = sum(sum(SAT.(desc_str).*areas_frac)) - ...
        sum(sum(SAT.(descdif).*areas_frac));
    % Save as array (to calculate mean easier)
    CO2mean_SATglob(i) = sum(sum(SAT.(desc_str).*areas_frac)) - ...
        sum(sum(SAT.(descdif).*areas_frac));
    
    CO2mean_SATgam(i) = SATgam2.(desc_str) - SATgam2.(descdif);
    CO2mean_SOSST(i) = SO_SST2.(desc_str) - SO_SST2.(descdif);
    CO2mean_DP(i) = DP_ACC2.(desc_str) - DP_ACC2.(descdif);
    CO2mean_AntP(i) = AntP2.(desc_str) - AntP2.(descdif);


end


% Calculate �95% uncertainty of the mean
CO2n = length(CO2mean_SATglob); %7

% Calculate the standard error
CO2SEx_SATglob = std(CO2mean_SATglob)/sqrt(CO2n);
CO2SEx_SATgam = std(CO2mean_SATgam)/sqrt(CO2n);
CO2SEx_SOSST = std(CO2mean_SOSST)/sqrt(CO2n);
CO2SEx_DP = std(CO2mean_DP)/sqrt(CO2n);
CO2SEx_AntP = std(CO2mean_AntP)/sqrt(CO2n);

% Calculate the �95% certainty bounds, with n-1 degrees of freedom
CO2uncertainty_SATglob = CO2SEx_SATglob * 2.447;
CO2uncertainty_SATgam = CO2SEx_SATgam * 2.447;
CO2uncertainty_SOSST = CO2SEx_SOSST * 2.447;
CO2uncertainty_DP = CO2SEx_DP * 2.447;
CO2uncertainty_AntP = CO2SEx_AntP * 2.447;
