% load_latlon

lon_lsm = double(ncread('../Getech/ancil/rup/rup_001.nc', 'longitude')); % lon_lsm isn't a very appropriate name, should really just be lon_high
lat_lsm = double(ncread('../Getech/ancil/rup/rup_001.nc', 'latitude'));
lon     = double(ncread('../EO_data/tdluno.pfclann.nc', 'longitude'));
lat     = double(ncread('../EO_data/tdluno.pfclann.nc', 'latitude'));
