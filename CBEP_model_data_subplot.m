% CBEP_model_data_subplot
% 
% This script calculates the uncertainty, plots the individual simulation pairs, 
% and plots the forcing mean for a predefined site, defined as such:
%    varice = NZmeanice;
%    vargeog = NZmeangeog;
%    varCO2 = NZmeanCO2;
%    varEOT = NZmeanEOT;
% 
%    post = (22.4 + 23.8)/2;
%    pre = (25.6 + 25.9)/2;
%    err = 1.05;
% 
%    axmax = 5;
%    axmin = -15;


%% Define some sefaults
fontnm = 'arial';
fontsz = 14;
red = [0.850000000000000,0.0872549042105675,0];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
orange = [0.917211353778839,0.625272333621979,0.498910665512085];
grey = [0.5 0.5 0.5];
lgrey = [0.8 0.8 0.8];
size1 = 5;
size2 = 10;


%% Calculate the �95% certainty bounds, with n-1 degrees of freedom
SEx_ice = std(varice(3,:))/sqrt(length(varice(1,:)));
uncertainty_ice = SEx_ice * 2.306; % n=9
uncertainty_ice_plot = uncertainty_ice/2;

SEx_geog = std(vargeog(3,:))/sqrt(length(vargeog(1,:)));
uncertainty_geog = SEx_geog * 2.306; % n=9
uncertainty_geog_plot = uncertainty_geog/2;

SEx_CO2 = std(varCO2(3,:))/sqrt(length(varCO2(1,:)));
uncertainty_CO2 = SEx_CO2 * 2.447; % n=7
uncertainty_CO2_plot = uncertainty_CO2/2;

SEx_EOT = std(varEOT(3,:))/sqrt(length(varEOT(1,:)));
uncertainty_EOT = SEx_EOT * 2.776; % n=5
uncertainty_EOT_plot = uncertainty_EOT/2;


%% Add shading for uncertainty
shade = fill([pre-20 pre+20 pre+20 pre-20],[post-20+err post+20+err post+20-err post-20-err],lgrey,'edgecolor','none');

% Setup axes
plot([axmin axmax],[post post],'k')
plot([pre pre],[axmin axmax],'k')
plot([axmin axmax],[axmax axmax],'k')
plot([axmin axmax],[axmin axmin],'k')
plot([axmin axmin],[axmin axmax],'k')
plot([axmax axmax],[axmin axmax],'k')

% Add correct magnitude of change line and zero change line
plot([pre-30 pre+20],[post-30 post+20],'-k')
plot([pre-30 pre+20],[post-30+(pre-post) post+20+(pre-post)],':k')


%% Plot individual simulation pairs
% Ice sheet effect
plot(varice(1,:),varice(2,:),'o','markersize',size1,'markerFaceColor',blue,'markerEdgeColor',blue)
% CO2 effect
plot(varCO2(1,:),varCO2(2,:),'o','markersize',size1,'markerFaceColor',red,'markerEdgeColor',red)
% Geog effect
plot(vargeog(1,:),vargeog(2,:),'o','markersize',size1,'markerFaceColor',grey,'markerEdgeColor',grey)
% combined effect
plot(varEOT(1,:),varEOT(2,:),'o','markersize',size1,'markerFaceColor',orange,'markerEdgeColor',orange)


%% Plot means and uncertainties
% Ice sheet effect
plot([mean(varice(1,:))-uncertainty_ice_plot mean(varice(1,:))+uncertainty_ice_plot],...
    [mean(varice(2,:))+uncertainty_ice_plot mean(varice(2,:))-uncertainty_ice_plot],'-','color',blue,'linewidth',3);
plot(mean(varice(1,:)),mean(varice(2,:)),'s','markersize',size2,'markerFaceColor',blue,'markerEdgeColor',blue)

% CO2 effect
plot([mean(varCO2(1,:))-uncertainty_CO2_plot mean(varCO2(1,:))+uncertainty_CO2_plot],...
    [mean(varCO2(2,:))+uncertainty_CO2_plot mean(varCO2(2,:))-uncertainty_CO2_plot],'-','color',red,'linewidth',3);
plot(mean(varCO2(1,:)),mean(varCO2(2,:)),'s','markersize',size2,'markerFaceColor',red,'markerEdgeColor',red)

% Geog effect
plot([mean(vargeog(1,:))-uncertainty_geog_plot mean(vargeog(1,:))+uncertainty_geog_plot],...
    [mean(vargeog(2,:))+uncertainty_geog_plot mean(vargeog(2,:))-uncertainty_geog_plot],'-','color',grey,'linewidth',3);
plot(mean(vargeog(1,:)),mean(vargeog(2,:)),'s','markersize',size2,'markerFaceColor',grey,'markerEdgeColor',grey)

% combined effect
plot([mean(varEOT(1,:))-uncertainty_EOT_plot mean(varEOT(1,:))+uncertainty_EOT_plot],...
    [mean(varEOT(2,:))+uncertainty_EOT_plot mean(varEOT(2,:))-uncertainty_EOT_plot],'-','color',orange,'linewidth',3);
plot(mean(varEOT(1,:)),mean(varEOT(2,:)),'s','markersize',size2,'markerFaceColor',orange,'markerEdgeColor',orange)


%% Tidy up figure
ylabel('SST post-EOT (�C)','fontname',fontnm, 'fontsize',fontsz)
xlabel('SST pre-EOT (�C)','fontname',fontnm, 'fontsize',fontsz)
plot(pre,post,'.k', 'markersize',40) % Plot actual proxy value
axis equal
ylim([axmin axmax])
xlim([axmin axmax])
set(gca, 'fontname',fontnm, 'fontsize',fontsz)
box on
