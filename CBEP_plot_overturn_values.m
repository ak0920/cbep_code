% CBEP_plot_overturn_values
% 
% This script generates Figure 3 for the CBEP paper
% 
% Each marker shape, colour, spacing and fill is defined by the simulation
% it comes from. There would be a clever coding way to do this I'm sure,
% but instead I have manually assigned them... Hence the length
% 

% Load data if necessary
if ~exist('SMOC')
    CBEP_load_overturn_values
end
if ~exist('geogmean_SOMLD')
    CBEP_load_overturn_changes
end

% Set some defaults
fontnm = 'arial';
fontsz = 14;
% Colours
red = [0.850000000000000,0.0872549042105675,0];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
grey = [0.6 0.6 0.6];
lgrey = [0.8 0.8 0.8];


%% Plotting

figure

%% SO MLD absolute:
subplot('position', [0.125 0.81 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[200 200 1200 1200],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[200 200 1200 1200],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};
% (See CBEP_load_mean_values.m)

% Chattian 4x runs:
plot(3,SO_MLD(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,SO_MLD(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,SO_MLD(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,SO_MLD(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2nI
plot(3.4,SO_MLD(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2eI
plot(3.5,SO_MLD(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2fI

% Rupelian 4x runs:
plot(2,SO_MLD(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4nI
plot(2.1,SO_MLD(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4fI

% Rupelian 2x runs:
plot(2.2,SO_MLD(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2nI
plot(2.3,SO_MLD(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2eI
plot(2.4,SO_MLD(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2fI

% Priabonian 4x runs:
plot(1.1,SO_MLD(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4nI
plot(1.2,SO_MLD(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4fI

% Priabonian 2x runs:
plot(1.3,SO_MLD(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2nI
plot(1.4,SO_MLD(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2fI


% Paul's runs:
plot(4.4,SO_MLD(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,SO_MLD(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,SO_MLD(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,SO_MLD(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,SO_MLD(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,SO_MLD(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,SO_MLD(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,SO_MLD(23),'^','markersize',10,'Color',red)% end3nC

% Tidy the figure
set(gca,'XTick',[],'Xlim',[0.9 5], 'Ylim',[200 1200], 'fontname',fontnm, 'fontsize',fontsz)
ylabel('Max S. Ocean MLD (m)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1,1100,'a', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Max SO MLD change:
subplot('position', [0.675 0.81 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Plot symbols once in order for legend
plot(1,mean(icemean_SOMLD),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% ice growth
plot(2,mean(geogmean_SOMLD),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% paleogeog
plot(3,mean(CO2mean_SOMLD),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% CO2
plot([1 1],[mean(icemean_SOMLD)-iceuncertainty_SOMLD mean(icemean_SOMLD)+iceuncertainty_SOMLD],'-','color',grey,'linewidth',5);
plot([1 1],[min(icemean_SOMLD) max(icemean_SOMLD)],'-','color',lgrey,'linewidth',2);

% Add zero line
plot([0 4],[0 0],'-k')

% Plot uncertainty bars with symbols on top
% Ice growth effect:
plot([1 1],[min(icemean_SOMLD) max(icemean_SOMLD)],'-','color',lgrey,'linewidth',2); % range
plot([1 1],[mean(icemean_SOMLD)-iceuncertainty_SOMLD mean(icemean_SOMLD)+iceuncertainty_SOMLD],'-','color',grey,'linewidth',5); % uncert
plot(1,mean(icemean_SOMLD),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean
% Paleogeog effect:
plot([2 2],[min(geogmean_SOMLD) max(geogmean_SOMLD)],'-','color',lgrey,'linewidth',2); % range
plot([2 2],[mean(geogmean_SOMLD)-geoguncertainty_SOMLD mean(geogmean_SOMLD)+geoguncertainty_SOMLD],'-','color',grey,'linewidth',5); % uncert
plot(2,mean(geogmean_SOMLD),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean
% CO2 effect:
plot([3 3],[min(CO2mean_SOMLD) max(CO2mean_SOMLD)],'-','color',lgrey,'linewidth',2); % range
plot([3 3],[mean(CO2mean_SOMLD)-CO2uncertainty_SOMLD mean(CO2mean_SOMLD)+CO2uncertainty_SOMLD],'-','color',grey,'linewidth',5); % uncert
plot(3,mean(CO2mean_SOMLD),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k') % mean

% Tidy up
ylabel(['Max. S. Ocean MLD change (m)'], 'fontname',fontnm, 'fontsize',fontsz)
box on
legend('Ice growth','Palaeogeog.','pCO_2','U_t','Range','Location',[0.675 0.04 0.2 0.1])
text(0.7,400,'b', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% North Atlantic MLD absolute:
subplot('position', [0.125 0.60 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[100 100 500 500],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[100 100 500 500],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x runs:
plot(3,NAtl_MLD(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,NAtl_MLD(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,NAtl_MLD(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,NAtl_MLD(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2nI
plot(3.4,NAtl_MLD(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2eI
plot(3.5,NAtl_MLD(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.05,0.05,0.05]) % cha2fI

% Rupelian 4x runs:
plot(2,NAtl_MLD(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup4nI
plot(2.1,NAtl_MLD(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup4fI

% Rupelian 2x runs:
plot(2.2,NAtl_MLD(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%,'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2nI
plot(2.3,NAtl_MLD(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2eI
plot(2.4,NAtl_MLD(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.5,0.5,0.5],'MarkerEdgeColor',[0.3,0.3,0.3]) % rup2fI

% Priabonian 4x runs:
plot(1.1,NAtl_MLD(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri4nI
plot(1.2,NAtl_MLD(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri4fI

% Priabonian 2x runs:
plot(1.3,NAtl_MLD(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri2nI
plot(1.4,NAtl_MLD(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)%[0.85,0.85,0.85],'MarkerEdgeColor',[0.55,0.55,0.55]) % pri2fI

% Paul's runs: 
plot(4.4,NAtl_MLD(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,NAtl_MLD(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,NAtl_MLD(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,NAtl_MLD(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,NAtl_MLD(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,NAtl_MLD(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,NAtl_MLD(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,NAtl_MLD(23),'^','markersize',10,'Color',red)% end3nC

% Tidy up
set(gca,'XTick',[],'Xlim',[0.9 5], 'YLim',[100 500], 'fontname',fontnm, 'fontsize',fontsz)
ylabel('Max. N. Atlantic MLD (m)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1,460,'c', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% North Atlantic MLD change:
subplot('position', [0.675 0.60 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero lone
plot([0 4],[0 0],'-k')

% Uncertainty bar and mean values
plot([1 1],[min(icemean_NAMLD) max(icemean_NAMLD)],'-','color',lgrey,'linewidth',2); % ice range
plot([1 1],[mean(icemean_NAMLD)-iceuncertainty_NAMLD mean(icemean_NAMLD)+iceuncertainty_NAMLD],'-','color',grey,'linewidth',5); % ice uncert
plot(1,mean(icemean_NAMLD),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k'); % ice mean
plot([2 2],[min(geogmean_NAMLD) max(geogmean_NAMLD)],'-','color',lgrey,'linewidth',2); % geog range
plot([2 2],[mean(geogmean_NAMLD)-geoguncertainty_NAMLD mean(geogmean_NAMLD)+geoguncertainty_NAMLD],'-','color',grey,'linewidth',5); % geog uncert
plot(2,mean(geogmean_NAMLD),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k'); % geog mean
plot([3 3],[min(CO2mean_NAMLD) max(CO2mean_NAMLD)],'-','color',lgrey,'linewidth',2); % CO2 range
plot([3 3],[mean(CO2mean_NAMLD)-CO2uncertainty_NAMLD mean(CO2mean_NAMLD)+CO2uncertainty_NAMLD],'-','color',grey,'linewidth',5); % CO2 uncert
plot(3,mean(CO2mean_NAMLD),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k'); % CO2 mean

% Tidy up
ylabel('Max. N. Atlantic MLD change (m)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7,140,'d', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Southern overturning absolute:
subplot('position', [0.125 0.39 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[10 10 40 40],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[10 10 40 40],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x runs:
plot(3,S_MOC(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,S_MOC(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,S_MOC(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,S_MOC(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2nI
plot(3.4,S_MOC(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2eI
plot(3.5,S_MOC(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2fI

% Rupelian 4x runs:
plot(2,S_MOC(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4nI
plot(2.1,S_MOC(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4fI

% Rupelian 2x runs:
plot(2.2,S_MOC(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2nI
plot(2.3,S_MOC(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2eI
plot(2.4,S_MOC(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2fI

% Priabonian 4x runs:
plot(1.1,S_MOC(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4nI
plot(1.2,S_MOC(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4fI

% Priabonian 2x runs:
plot(1.3,S_MOC(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2nI
plot(1.4,S_MOC(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2fI

% Paul's runs:
plot(4.4,S_MOC(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,S_MOC(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,S_MOC(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,S_MOC(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,S_MOC(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,S_MOC(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,S_MOC(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,S_MOC(23),'^','markersize',10,'Color',red)% end3nC

% Tidy up
set(gca,'XTick',[],'Xlim',[0.9 5],'Ylim',[10 40],'fontname',fontnm, 'fontsize',fontsz)
ylabel('Max. southern overturning (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1,37,'e', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Southern overturning change:
subplot('position', [0.675 0.39 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero line
plot([0 4],[0 0],'-k')

% Plot uncertainty bars and mean values:
plot([1 1],[min(icemean_SMOC) max(icemean_SMOC)],'-','color',lgrey,'linewidth',2);% ice growth range
plot([1 1],[mean(icemean_SMOC)-iceuncertainty_SMOC mean(icemean_SMOC)+iceuncertainty_SMOC],'-','color',grey,'linewidth',5);% ice growth uncert
plot(1,mean(icemean_SMOC),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% ice growth mean
plot([2 2],[min(geogmean_SMOC) max(geogmean_SMOC)],'-','color',lgrey,'linewidth',2);% paleogeog range
plot([2 2],[mean(geogmean_SMOC)-geoguncertainty_SMOC mean(geogmean_SMOC)+geoguncertainty_SMOC],'-','color',grey,'linewidth',5);% paleogeog uncert
plot(2,mean(geogmean_SMOC),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% paleogeog mean
plot([3 3],[min(CO2mean_SMOC) max(CO2mean_SMOC)],'-','color',lgrey,'linewidth',2);% CO2 range
plot([3 3],[mean(CO2mean_SMOC)-CO2uncertainty_SMOC mean(CO2mean_SMOC)+CO2uncertainty_SMOC],'-','color',grey,'linewidth',5);% CO2 uncert
plot(3,mean(CO2mean_SMOC),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% CO2 mean

% Tidy up
ylabel('Max. southern overturning change (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7,17,'f', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Northern overturning absolute:
subplot('position', [0.125 0.18 0.525 0.17])
hold on

% Shading the alternate Stages
prishade = fill([0.9 1.7 1.7 0.9],[0 0 20 20],[0.8 0.8 0.8],'edgecolor','k');
chashade = fill([2.7 3.75 3.75 2.7],[0 0 20 20],[0.8 0.8 0.8],'edgecolor','k');

% For reference (all runs including Paul's):
% desc = {'cha4nI','cha4eI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI',...
%     'rup2nI','rup2eI','rup2fI','pri4nI','pri4fI','pri2nI','pri2fI',...
%     'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};

% Chattian 4x runs:
plot(3,N_MOC(1),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4nI
plot(3.1,N_MOC(2),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4eI
plot(3.2,N_MOC(3),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% cha4fI

% Chattian 2x runs:
plot(3.3,N_MOC(4),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2nI
plot(3.4,N_MOC(5),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2eI
plot(3.5,N_MOC(6),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% cha2fI

% Rupelian 4x runs:
plot(2,N_MOC(7),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4nI
plot(2.1,N_MOC(8),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% rup4fI

% Rupelian 2x runs:
plot(2.2,N_MOC(9),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2nI
plot(2.3,N_MOC(10),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2eI
plot(2.4,N_MOC(11),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% rup2fI

% Priabonian 4x runs:
plot(1.1,N_MOC(12),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4nI
plot(1.2,N_MOC(13),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% pri4fI

% Priabonian 2x runs:
plot(1.3,N_MOC(14),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2nI
plot(1.4,N_MOC(15),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% pri2fI

% Paul's runs:
plot(4.4,N_MOC(16),'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2fO
plot(4.8,N_MOC(17),'s','markersize',10,'Color',blue)% end2fC
plot(4.3,N_MOC(18),'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue)% end2nO
plot(4.7,N_MOC(19),'^','markersize',10,'Color',blue)% end2nC
plot(4.2,N_MOC(20),'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3fO
plot(4.6,N_MOC(21),'s','markersize',10,'Color',red)% end3fC
plot(4.1,N_MOC(22),'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red)% end3nO
plot(4.5,N_MOC(23),'^','markersize',10,'Color',red)% end3nC

% Set bottom x-axis labels
names = {'Priabonian'; 'Rupelian'; 'Chattian'; 'Rupelian (alt.)'};
set(gca,'xtick',[1.3, 2.2, 3.225, 4.4],'xticklabel',names,'Xlim',[0.9 5], 'fontname',fontnm, 'fontsize',fontsz)

% Tidy up
ylabel('Max. northern overturning (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(1,18,'g', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Northern overturning change:
subplot('position', [0.675 0.18 0.2 0.17])
set(gca,'YAxisLocation','right', 'fontname',fontnm, 'fontsize',fontsz,'XTick',[],'Xlim',[0.5 3.5])
hold on 

% Add zero line
plot([0 4],[0 0],'-k')

% Add mean values and uncertainty
plot([1 1],[min(icemean_NMOC) max(icemean_NMOC)],'-','color',lgrey,'linewidth',2);% ice growth range
plot([1 1],[mean(icemean_NMOC)-iceuncertainty_NMOC mean(icemean_NMOC)+iceuncertainty_NMOC],'-','color',grey,'linewidth',5);% ice growth uncert
plot(1,mean(icemean_NMOC),'v','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% ice growth mean
plot([2 2],[min(geogmean_NMOC) max(geogmean_NMOC)],'-','color',lgrey,'linewidth',2);% geog range
plot([2 2],[mean(geogmean_NMOC)-geoguncertainty_NMOC mean(geogmean_NMOC)+geoguncertainty_NMOC],'-','color',grey,'linewidth',5);% geog uncert
plot(2,mean(geogmean_NMOC),'o','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% % geog mean
plot([3 3],[min(CO2mean_NMOC) max(CO2mean_NMOC)],'-','color',lgrey,'linewidth',2);% CO2 range
plot([3 3],[mean(CO2mean_NMOC)-CO2uncertainty_NMOC mean(CO2mean_NMOC)+CO2uncertainty_NMOC],'-','color',grey,'linewidth',5);% CO2 uncert
plot(3,mean(CO2mean_NMOC),'d','markersize',10,'MarkerFaceColor','k','MarkerEdgeColor','k')% CO2 mean

% Tidy up
ylabel('Max. northern overturning change (Sv)', 'fontname',fontnm, 'fontsize',fontsz)
box on
text(0.7,8,'h', 'fontname',fontnm, 'fontsize',18,'fontweight','bold')


%% Manually create legend for left hand column
leg1ax = axes('Position', [0.125 0.0175 0.525 0.1]);
hold on
plot(2.2,2.8,'^','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.6,2.8,'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.2,1.8,'s','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red) 
plot(2.6,1.8,'s','markersize',10,'MarkerFaceColor',blue,'MarkerEdgeColor',blue) 
plot(2.2,0.8,'^','markersize',10,'MarkerFaceColor',red,'MarkerEdgeColor',red) 
plot(2.6,0.8,'^','markersize',10,'Color',red)
text(1, 3,'Shape:', 'fontname',fontnm, 'fontsize',fontsz)
text(1, 2,'Colour:', 'fontname',fontnm, 'fontsize',fontsz)
text(1, 1,'Fill:', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 3,'No ice / Glaciated', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 2,'High / Low pCO_2', 'fontname',fontnm, 'fontsize',fontsz)
text(3, 1,'Open / Closed Drake Passage', 'fontname',fontnm, 'fontsize',fontsz)
set(gca,'xlim',[0.75 7],'ylim',[0 4],'xtick',[],'ytick',[])
% axis off
box on

% Final tidy up
set(gcf, 'color', 'w');
x0=10;
y0=10;
width=800;
height=1200;
set(gcf,'units','points','position',[x0,y0,width,height])

