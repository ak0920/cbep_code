% CBEP_SATmeandif
% 
% This script generates the mean spatial change for all pairs of simulations 
% for AIS growth/palaeogeographic change/CO2 drop.
% 

% Load data
CBEP_loaddata


%% Mean effect of ice growth

% Identifiers for glaciated icehouse simulations with a unglaciated pair
desc = {'cha4eI','cha4fI','rup4fI','cha2fI','cha2eI','rup2fI','rup2eI','pri4fI','pri2fI'};

% Make empty array to store each ice comparison
SATannicedif = nan(73,96,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (glaciated identifiers) into useable string
    desc_cell = desc(i);
    desc_ice = sprintf('%s',desc_cell{:});
    
    % Find unglaciated identifiers
    desc_noice = [desc_ice(1:4),'n',desc_ice(6)];

    % Find difference
    SATannicedif(:,:,i) = SAT.(desc_ice) - SAT.(desc_noice);
    
end

% Find mean of all pairs
SATmeanicedif = mean(SATannicedif,3);
% Find standard deviation of all pairs
SATstdicedif = std(SATannicedif,0,3);


%% Mean effect of paleogeographic change

% Identifiers for icehouse stage simulations with a previous stage pair
desc = {'cha4nI','cha4fI','cha2nI','cha2eI','cha2fI','rup4nI','rup4fI','rup2nI','rup2fI'};

% Make empty array to store each stage comparison
SATanngeogdif = nan(73,96,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (second stage identifiers) into useable string
    desc_cell = desc(i);
    desc_geog = sprintf('%s',desc_cell{:});
    
   % Find previous equivalent stage
    if desc_geog(1) == 'c'
        desc_geog2 = ['rup',desc_geog(4:6)];
    else if desc_geog(1) == 'r'
            desc_geog2 = ['pri',desc_geog(4:6)];
        end
    end
    
    % Find difference
    SATanngeogdif(:,:,i) = SAT.(desc_geog) - SAT.(desc_geog2);
    
end

% Find mean of all pairs
SATmeangeogdif = mean(SATanngeogdif,3);
% Find standard deviation of all pairs
SATstdgeogdif = std(SATanngeogdif,0,3);


%% Mean effect of CO2 drop

% Identifiers for 2x icehouse simulations with a 4x CO2 pair
desc = {'cha2nI','cha2eI','cha2fI','rup2nI','rup2fI','pri2nI','pri2fI'};

% Make empty array to store each CO2 comparison
SATannCO2dif = nan(73,96,length(desc));

% Loop through each pair to find difference between them
for i = 1:length(desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = desc(i);
    desc_2x = sprintf('%s',desc_cell{:});
    
    % Find 4x CO2 identifiers
    desc_4x = [desc_2x(1:3),'4',desc_2x(5:6)];

    % Find difference
    SATannCO2dif(:,:,i) = SAT.(desc_2x) - SAT.(desc_4x);

end

% Find mean of all pairs
SATmeanCO2dif = mean(SATannCO2dif,3);
% Find standard deviation of all pairs
SATstdCO2dif = std(SATannCO2dif,0,3);
